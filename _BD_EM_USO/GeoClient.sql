CREATE TABLE public.empresa
(
  id_empresa serial NOT NULL,
  nome_empresa character varying(50) NOT NULL,
  cnpj_empresa character varying(18),
  CONSTRAINT id_empresa PRIMARY KEY (id_empresa)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.empresa
  OWNER TO postgres;

CREATE TABLE public.colaborador
(
  id_colab serial NOT NULL,
  id_empresa integer NOT NULL,
  nome_colab character varying(200) NOT NULL,
  email_colab character varying(150) NOT NULL,
  usuario_colab character varying(20) NOT NULL,
  senha_colab character varying(32) NOT NULL,
  mac_colab character varying(17) NOT NULL,
  so_colab character varying(30) NOT NULL,
  versao_colab character varying(10) NOT NULL,
  numero_colab character varying(20) NOT NULL,
  validacao_colab boolean NOT NULL DEFAULT false,
  chave_verif character varying(40),
  CONSTRAINT id_colab PRIMARY KEY (id_colab),
  CONSTRAINT id_empresa FOREIGN KEY (id_empresa)
      REFERENCES public.empresa (id_empresa) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.colaborador
  OWNER TO postgres;

CREATE TABLE public.administrador
(
  id_adm serial NOT NULL,
  id_empresa integer NOT NULL,
  nome_adm character varying(200) NOT NULL,
  usuario_adm character varying(20),
  senha_adm character varying(32) NOT NULL,
  email_adm character varying(150),
  CONSTRAINT id_adm PRIMARY KEY (id_adm),
  CONSTRAINT id_empresa FOREIGN KEY (id_empresa)
      REFERENCES public.empresa (id_empresa) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.administrador
  OWNER TO postgres;

CREATE TABLE public.localizacao
(
  id_loca serial NOT NULL,
  nome_cliente character varying(200) NOT NULL,
  logradoro_cliente character varying(50),
  numero_cliente character varying(8) NOT NULL,
  complemento_cliente character varying(100),
  bairro_cliente character varying(100) NOT NULL,
  cep_cliente character varying(9) NOT NULL,
  cidade_cliente character varying(100) NOT NULL,
  estado_cliente character varying(2) NOT NULL,
  rua_cliente character varying(200) NOT NULL,
  longitude_cliente character varying(20),
  latitude_cliente character varying(20),
  satelites_ativos integer,
  precisao_loca character varying(20),
  id_empresa integer,
  id_colab integer,
  status_visita character varying(50),
  CONSTRAINT id_loca PRIMARY KEY (id_loca),
  CONSTRAINT id_empresa FOREIGN KEY (id_empresa)
      REFERENCES public.empresa (id_empresa) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.localizacao
  OWNER TO postgres;

CREATE TABLE public.visitas
(
  id_visita serial NOT NULL,
  id_colab integer NOT NULL,
  id_loca integer NOT NULL,
  visita_feita character varying(3) NOT NULL,
  latitude_visi character varying(20),
  longitude_visi character varying(20),
  data_atribuida date,
  data_retorn date,
  data_aprov date,
  satelitesativos_vis integer,
  precisao_vis character varying(20),
  status_vis character varying(25),
  logradouro_vis character varying(40),
  numero_vis character varying(6),
  bairro_vis character varying(100),
  cidade_vis character varying(60),
  dt_rejeicao date,
  rua_vis character varying(300),
  CONSTRAINT id_visita PRIMARY KEY (id_visita),
  CONSTRAINT id_colab FOREIGN KEY (id_colab)
      REFERENCES public.colaborador (id_colab) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT id_loca FOREIGN KEY (id_loca)
      REFERENCES public.localizacao (id_loca) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.visitas
  OWNER TO postgres;

