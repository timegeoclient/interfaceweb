<?php

/*
|--------------------------------------------------------------------------
| Configurações Básicas
|--------------------------------------------------------------------------
|
*/
header('Content-Type: text/html; charset=UTF-8'); // Define o charset
date_default_timezone_set('America/Sao_Paulo'); // Define o fuso horario

define("ABSPATH", dirname( __FILE__ ) . "/" ); // Caminho absoluto para a raiz

define("URL_HOME", "http://127.0.0.1/geoclient/"); // URL da home
define("URL_BASE_ADMIN", "http://".$_SERVER['SERVER_NAME']."/geoclient/admin/"); // URL base

define("DEBUG_GLOBAL", true);


/*
|--------------------------------------------------------------------------
| Verifica se a versão do PHP é superior a '5.3.10'
|--------------------------------------------------------------------------
|
*/
if (version_compare(PHP_VERSION, '5.3.10', '<'))
{
	die('Erro! a aplicação deve utilizar uma versão PHP igual ou superior a 5.3.10!');
}