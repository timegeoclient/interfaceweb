<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Erro 404</title>

    <style type="text/css">
        body{
            background: #eee;
            text-align: center;
            font-family: Arial;
        }

        h1{
            margin-top: 50px;
        }
    </style>

</head>
<body>

    <h1>Erro 404! Página não encontrada.</h1>

</body>
</html>