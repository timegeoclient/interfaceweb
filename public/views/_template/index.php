<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>

    <style type="text/css">
        body{
            background: #eee;
            text-align: center;
            font-family: Arial;
        }

        h1{
            margin-top: 50px;
        }
    </style>

</head>
<body>

    <h1>Seja bem-vindo ao site!</h1>
    <a href="admin/login.php">Faça login</a>

</body>
</html>