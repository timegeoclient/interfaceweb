<?php

/**
 * Pattern: Front Controller
 *
 * Esse index.php atua como um script centralizador de toda a parte PUBLIC da aplicação
 *
 * Ao executar a classe App.class.php,
 * Decidirá qual página será exibida
 *
 * @version 1.0
 */

/*
|--------------------------------------------------------------------------
| Arquivos necessarios para a aplicação funcionar
|  - Arquivo de configuração
|  - Funções principais da aplicação  ( Autoload, etc )
|--------------------------------------------------------------------------
|
*/
if (!file_exists('../config.php'))
	die("Error!! The configuration file can not be found.");

require_once '../config.php'; // Configurações Básicas
require_once ABSPATH.'core/functions/Main.php'; // Funções


/*
|--------------------------------------------------------------------------
| Verifica se o debug para o Public esta habilitado
|--------------------------------------------------------------------------
|
*/
if ( Config::get('PUBLIC_DEBUG') === false ) {

	# Esconde todos os erros
	error_reporting(0);
	ini_set("display_errors", 0);

} else {

	# Mostra todos os erros
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}


/*
|--------------------------------------------------------------------------
| Inicializa uma sessão
|--------------------------------------------------------------------------
|
*/
WolfSession::init();


/*
|--------------------------------------------------------------------------
| Verifica se aplicação encontra-se em manutenção
|--------------------------------------------------------------------------
|
*/
if ( Config::get('PUBLIC_MAINTENANCE') === true ) {

	#require_once ABSPATH . '/includes/_templates/maintenance/public.php';
	die("A aplicação PUBLIC encontra-se em manutenção!");
	exit();
}


/*
|--------------------------------------------------------------------------
| Executa a aplicação
|--------------------------------------------------------------------------
|
*/
$app =  new App('public');

?>