<?php

/*
|--------------------------------------------------------------------------
| Configurações Globais
|--------------------------------------------------------------------------
|
*/
if ( !defined('ABSPATH')) exit; // Evita acesso direto a este arquivo

return array(

    /**
     * Nome do projetos
     */
    "NAME_PROJECT" =>"GeoClient",

    /**
     * A aplicação Admin ou Public encontra-se em manutenção ?
     */
    "ADMIN_MAINTENANCE" => false,
    "PUBLIC_MAINTENANCE" => false,

    "ADMIN_DEBUG" => true, // Debug apenas no admin
    "PUBLIC_DEBUG" => true, // Debug apenas no public
    "LOGGER" => true, // Modificar o valor para true caso queira ativar o registro de logs

    "SALT" => "&7HX_#M{ob4XX%K}bTFg", // Valor secreto incrementado em algumas criptografias
    "HTTPS" => false, // Definir o valor para true caso a aplicação esteja usando HTTPS
    "NAME_SESSION" => "_ses_", // Nome personalizado para sessões

    "COOKIE_DOMAIN" => "127.0.0.1",

    "ADMIN_GET_CONTROL" => "control", //Key do GET para o controle do Admin
    "ADMIN_GET_METHOD" => "method", //Key do GET para o metodo do Admin

    "ABSPATH_CORE" => ABSPATH . "core/", // Caminho absoluto para o centro da aplicação
    "ABSPATH_LOGS" => ABSPATH . "core/logs/", // Caminho absoluto para a pasta de Logs
    "ABSPATH_LIBRARIES" => ABSPATH . "core/class/libraries/", // Caminho absoluto para a pasta de bibliotecas

    "ABSPATH_CONTROLS_ADMIN" => ABSPATH . "admin/controls/", // Caminho absoluto para a pasta dos controles do Admin
    "ABSPATH_MODELS_ADMIN" => ABSPATH . "admin/models/", // Caminho absoluto para a pasta dos modelos do Admin
    "ABSPATH_VIEWS_ADMIN" => ABSPATH . "admin/views/", // Caminho absoluto para a pasta das visões do Admin

    "ABSPATH_CONTROLS_PUBLIC" => ABSPATH . "public/controls/", // Caminho absoluto para a pasta dos controles do Public
    "ABSPATH_MODELS_PUBLIC" => ABSPATH . "public/models/", // Caminho absoluto para a pasta dos modelos do Public

    "ABSPATH_PUBLIC_TEMPLATE" => ABSPATH . "public/views/_template/", // Caminho absoluto para a pasta do Template Public
    "ABSPATH_CORE_TEMPLATE" => ABSPATH . "core/templates/", // Pasta de templates central da aplicação (tempate de envio de email, etc)

    "URI_TEMPLATE_ADMIN" => URL_HOME . "admin/views/_template/", // URL até a pasta do template Admin
    "URI_TEMPLATE_PUBLIC" => URL_HOME . "public/views/_template/", // URL até a pasta do template Public

    "ABSPATH_UP_ADMIN" => ABSPATH . "admin/uploads/", // Caminho absoluto até a pasta de Uploads do Admin
    "ABSPATH_UP_PUBLIC" => ABSPATH . "public/uploads/", // Caminho absoluto até a pasta de Uploads do Public
    "URI_UP_ADMIN" => URL_HOME . "admin/uploads/", // URL até a pasta de Uploads do Admin
    "URI_UP_PUBLIC" => URL_HOME . "public/uploads/", // URL até a pasta de Uploads do Public

    "COL_CONFIRM_URL" => URL_HOME . "admin/colab-verification.php", // URL de verificação de colaborador

    /**
     * Configurações de Email - SMTP
     */
    "EMAIL_SMTP_HOST" => "smtp.gmail.com",
    "EMAIL_SMTP_AUTH" => true,
    "EMAIL_SMTP_USERNAME" => "appfran10@gmail.com",
    "EMAIL_SMTP_PASSWORD" => "",
    "EMAIL_SMTP_ENCRYPTION" => "tls",
    "EMAIL_SMTP_PORT" => 587, // 587 => Porta do Gmail ( outra porta geral 465)

    /**
     * Dados de conexão com o banco de dados
     */
    "PREFIX_TABLE" => "ptwp_", // Prefixo das tabelas
    "HOSTNAME" => "localhost", // Nome do host da base de dados
    "DB_NAME" => "v2", // Nome do DB
    "DB_USER" => "postgres", // Usuário do DB
    "DB_PASSWORD" => "admin", // Senha do DB
    "DB_DRIVER" => "pgsql", // Driver do DB
    "DB_CHARSET" => "utf8", // Charset da conexão PDO

    /**
     * API Google Maps Geocoding
     */
    "API_GOOGLE_GEOCODING" => "AIzaSyAb0zQJK7tgTsZGH7C837MxoCo8ioTOD88"
);
