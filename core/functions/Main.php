<?php
/*
|--------------------------------------------------------------------------
| Função para carregar automaticamente todas as classes
| O nome do arquivo deverá ser NomeDaClasse.class.php
|--------------------------------------------------------------------------
|
*/
if ( !defined('ABSPATH')) exit; // Evita acesso direto a este arquivo

function __autoload( $nameClass )
{
    $require_class = false;

    $paths = array(
        ABSPATH.'core/class/'
        );

    // Se a classe existir ela será incluída
    foreach ($paths as $path)
    {
        if (file_exists("{$path}/{$nameClass}.class.php"))
        {
            require_once "{$path}/{$nameClass}.class.php";
            $require_class = true;
        }
    }

    // Se a classe não tiver sido encontrada
    if ( $require_class === false ) {

        # Template de serviço indisponível
        //require_once ABSPATH . 'includes/_templates/page-errors/504.php';

        # Exibe qual classe não foi encontrada
        if ( DEBUG_GLOBAL === true ) {
            die("A classe {$nameClass} não pode ser encontrada!!");
        }
    }
}// __autoload


/*
|--------------------------------------------------------------------------
| Obtém o IP do usuário
|--------------------------------------------------------------------------
|
*/
function get_ip() {

    $ip_keys = array('HTTP_CLIENT_IP',
                    'HTTP_X_FORWARDED_FOR',
                    'HTTP_X_FORWARDED',
                    'HTTP_X_CLUSTER_CLIENT_IP',
                    'HTTP_FORWARDED_FOR',
                    'HTTP_FORWARDED',
                    'REMOTE_ADDR');

    foreach ($ip_keys as $key) {

        if (array_key_exists($key, $_SERVER) === true) {

            foreach (explode(',', $_SERVER[$key]) as $ip) {

                $ip = trim($ip);

                // Valida o IP
                if ( WolfFilter::validate_ip($ip) ) {
                    return $ip;
                }
            }
        }
    }

    return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
}


/*
|--------------------------------------------------------------------------
| Captura a URL atual
|--------------------------------------------------------------------------
|
*/
function get_url_actual()
{
    $dominio = $_SERVER['HTTP_HOST'];
    $url = "http://" . $dominio. $_SERVER['REQUEST_URI'];

    return (WolfFilter::sanitize_url( $url) );
}


/*
|--------------------------------------------------------------------------
| CRIPTOGRAFIA
|--------------------------------------------------------------------------
|
*/
function encrypt_pass( $value )
{
    $value .= Config::get("SALT");
    return md5( $value );
}


/*
|--------------------------------------------------------------------------
| RETORNA AS LINHAS DE UM ARQUIVO .CSV EM FORMA DE ARRAY
|--------------------------------------------------------------------------
|
*/
function read_CSV($csvfile) {
    $csv = Array();
    $rowcount = 0;
    if (($handle = fopen($csvfile, "r")) !== FALSE) {
        $max_line_length = defined('MAX_LINE_LENGTH') ? MAX_LINE_LENGTH : 10000;
        $header = fgetcsv($handle, $max_line_length);
        $header_colcount = count($header);
        while (($row = fgetcsv($handle, $max_line_length)) !== FALSE) {
            $row_colcount = count($row);
            if ($row_colcount == $header_colcount) {
                $entry = array_combine($header, $row);
                $csv[] = $entry;
            }
            else {
                error_log("csvreader: Invalid number of columns at line " . ($rowcount + 2) . " (row " . ($rowcount + 1) . "). Expected=$header_colcount Got=$row_colcount");
                return false;
            }
            $rowcount++;
        }
        //echo "Totally $rowcount rows found\n";
        fclose($handle);
    }
    else {
        error_log("csvreader: Could not read CSV \"$csvfile\"");
        return false;
    }
    return $csv;
}


/*
|--------------------------------------------------------------------------
| Template para mensagens de sucesso e erro
|--------------------------------------------------------------------------
|
*/
// Estilo Default: Erro
function msg_error_default($msg)
{
    return '<div class="alert alert-danger" role="alert">'.$msg.'</div>';
}

// Estilo Default: Sucesso
function msg_success_default($msg)
{
    return '<div class="alert alert-success" role="alert">'.$msg.'</div>';
}