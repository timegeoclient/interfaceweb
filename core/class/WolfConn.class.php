<?php
/**
 * Classe de conexão com banco de dados usando PDO no padrão Singleton
 * Gerencia também a Transação das operações com o banco de dados
 *
 * @author Françuel Soares <fs.francuelsoares@gmail.com>
 * @copyright Copyright (c) 2016, Françuel Soares <fs.francuelsoares@gmail.com>
 * @license http://opensource.org/licenses/ISC ISC License (ISC)
 * @link https://github.com/FrancuelSoares
 * @since 2016-01-01
 * @version 1.0
 */
final class WolfConn
{


    /**
     * @var $sql armazena a conexão PDO
     * @access protected static
     *
     */
    protected static $conn;



    /*
     * Private construct - garante que a classe só possa ser instanciada internamente.
     */
    private function __construct()
    {
        try
        {
            # Verifica qual o tipo do banco de dados
            # Atribui o objeto PDO à variável $conn
            switch ( Config::get("DB_DRIVER") ) {
                case 'mysql':
                    self::$conn = new PDO( Config::get("DB_DRIVER").":host=".Config::get("HOSTNAME")."; dbname=".Config::get("DB_NAME")."; charset=".Config::get("DB_CHARSET"), Config::get("DB_USER"), Config::get("DB_PASSWORD") );
                    break;
                case 'pgsql':
                    self::$conn = new PDO( Config::get("DB_DRIVER").":host=".Config::get("HOSTNAME")."; dbname=".Config::get("DB_NAME")."", Config::get("DB_USER"), Config::get("DB_PASSWORD"));
                    break;
                default:
                    die("Tipo de banco de dados não suportado!!");
                    break;
            }

            # Garante que o PDO lance exceções durante erros.
            self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            # Codificação dos dados Armazenados
            //self::$conn->exec('SET NAMES ' . Config::get("DB_CHARSET") );

            # Inicia a transação
            self::$conn->beginTransaction();
        }

        catch (PDOException $e)
        {
            # Mostra o erro
            if ( DEBUG_GLOBAL === true ) {
                echo $e->getMessage();
            }

            # Salva o log de erro
            if ( Config::get("LOGGER") === true ) {
                WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/conn/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' );
            }

            # Em caso de erro, a aplicação exibe uma página de serviço indisponível
            //require_once ABSPATH . 'includes/_templates/page-errors/504.php';
            die("Falha na conexão com o banco de dados!!");
            exit();
        }
    }



    /*
    * Abre uma conexao com o banco de dados | Metodo estático - acessível sem instanciação.
    */
    public static function db_connect()
    {
        // Garante uma única instância. Se não existe uma conexão, criamos uma nova.
        if ( !self::$conn )
        {
            new WolfConn();
        }
    }



    /*
    * Recupera a conexao ativa com o banco de dados a transação
    */
    public static function get_db_connection()
    {
        return self::$conn;
    }



    /*
     * Desfaz todas operações realizadas na transação
     */
    public static function db_rollback()
    {
        if ( self::$conn )
        {
            # Desfaz as operações
            self::$conn->rollback();

            # Fecha a conexão
            self::$conn = NULL;
        }
    }



    /*
    * Fecha a conexao com o banco de dados e Aplica todas operações realizadas na transação
    */
    public static function db_commit()
    {
        if ( self::$conn )
        {
            # Aplica operações
            self::$conn->commit();

            # Fecha a conexão
            self::$conn = null;
        }
    }
}