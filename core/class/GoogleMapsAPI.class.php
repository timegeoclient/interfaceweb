<?php
/**
 * Class Abstrata para trabalhar com a API do Google Maps
 *
 * @version 1.0
 */
abstract class GoogleMapsAPI {

    /**
     * @var Armazena a URL de consulta
     * @access protected
     */
    protected $url;

    /**
     * @var Armazena o endereço da consulta
     * @access protected
     */
    protected $address;

    /**
     * @var array: armazena mensagen de erros
     * @access public
     *
     */
    public $msgErrorGM = array();

    /**
     * @var string: Armazena a chave de para o funcionamento da API
     *
     */
    const KEY_API = 'AIzaSyAb0zQJK7tgTsZGH7C837MxoCo8ioTOD88';
}