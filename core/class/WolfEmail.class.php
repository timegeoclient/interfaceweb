<?php
/**
 * Classe para trabalhar com envio de emails
 *
 * @author Françuel Soares <fs.francuelsoares@gmail.com>
 * @copyright Copyright (c) 2016, Françuel Soares <fs.francuelsoares@gmail.com>
 * @license http://opensource.org/licenses/ISC ISC License (ISC)
 * @link https://github.com/FrancuelSoares
 * @since 2016-01-01
 * @version 1.0
 */
final class WolfEmail {

	/**
     * constructor
     *
     * @access private
     */
    private function __construct() {}


    /**
     * Envia um EMAIL
     *
     * @param $data_email array
     * @return boll
     */
    public static function send_email( array $data_email )
    {
        if (
            empty($data_email) ||
            !array_key_exists('EMAIL_USE_SMTP', $data_email) ||
            !is_bool($data_email['EMAIL_USE_SMTP']) ||
            !array_key_exists('FromEmail', $data_email) ||
            !array_key_exists('FromName', $data_email) ||
            !array_key_exists('AddressEmail', $data_email) ||
            !array_key_exists('AddressName', $data_email) ||
            !array_key_exists('subject', $data_email) ||
            !array_key_exists('messageBody', $data_email) ||
            !array_key_exists('messageAltBody', $data_email)
            )
        {
            die('O array de configurações para envio de email não esta configurado corretamente!');
        }

        # Inclui a biblioteca PHP-Mailer
        require_once Config::get("ABSPATH_LIBRARIES") . 'php-mailer/class.phpmailer.php';
        require_once Config::get("ABSPATH_LIBRARIES") . 'php-mailer/class.smtp.php';

        $mail = new PHPMailer;

        // Verifica se esta usando SMTP
        if ( $data_email['EMAIL_USE_SMTP'] ) {

            // Define os dados do servidor e tipo de conexão
            $mail->IsSMTP(); // Define que a mensagem será do tipo SMTP
            $mail->Host = Config::get("EMAIL_SMTP_HOST"); // Endereço do servidor SMTP
            $mail->SMTPAuth = Config::get("EMAIL_SMTP_AUTH"); // Usa autenticação SMTP? (opcional)
            $mail->Username = Config::get("EMAIL_SMTP_USERNAME"); // Usuário do servidor SMTP
            $mail->Password = Config::get("EMAIL_SMTP_PASSWORD"); // Senha do servidor SMTP

            if (defined(Config::get("EMAIL_SMTP_ENCRYPTION"))) {
                $mail->SMTPSecure = Config::get("EMAIL_SMTP_ENCRYPTION"); // Enable TLS encryption, `ssl` also accepted
            }

            $mail->Port = Config::get("EMAIL_SMTP_PORT"); // TCP port to connect to
        }

        // Se não, usará o IsMail
        else {
            $mail->IsMail();
        }

        $mail->setFrom( $data_email['FromEmail'], $data_email['FromName']); // Define o remetente
        $mail->addAddress( $data_email['AddressEmail'], $data_email['AddressName'] ); // Define o destinatário

        // Define os dados técnicos da Mensagem
        $mail->isHTML(true); // Define que o e-mail será enviado como HTML
        $mail->CharSet = 'utf-8'; // Charset da mensagem (opcional)

        $mail->Subject = $data_email['subject']; // Assunto da mensagem
        $mail->Body    = $data_email['messageBody']; // Corpo da Mensagem
        $mail->AltBody = $data_email['messageAltBody']; // Corpo em texto simples para clientes de correio não-HTML

        // Mensagem enviada com sucesso
        if( $mail->send() ) {

            # Sucesso :D
            return true;
        }

        // Erro no envio da mensagem
        else {

            # Mostra o erro
            if ( DEBUG_GLOBAL === true ) {
                echo $mail->ErrorInfo;
            }

            # Salva o log de erro
            if ( Config::get("LOGGER") === true ) {
                WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/email/', 'Classe ' . __CLASS__  . ': ' . $mail->ErrorInfo, 'html' );
            }

            # Erro :X
            return false;
        }
    }
}