<?php

/**
 * Recupera os valores das configurações globais
 *
 * @version 1.0
 */
final class Config {

    /**
     * Array com as configurações
     *
     * @var array
     */
    public static $config = null;


    /**
     * constructor
     *
     * @access private
     */
    private function __construct() {}

    /**
     * Captura o valor da chave correspondente
     *
     * @param $key string
     * @return string|null
     * @throws Exception se o arquivo de configuração não existir
     */
    public static function get( $key ){

        if (!self::$config) {

	        $file = ABSPATH . 'core/config/global.php';

			if (!file_exists($file)) {
				die("Arquivo de configurações não encontrado!!<br>");
			}

	        self::$config = require $file . "";
        }

        return isset(self::$config[$key]) ? self::$config[$key]: null;
    }
}
