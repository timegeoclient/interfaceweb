<?php

/**
 * Application - Gerencia qual página(controle) e ação(método) serão executados no Admin ou Public
 *
 * @version 1.0
 *
 */
final class App
{

	/**
	 * @var string: receberá o valor do controle do Admin
	 * @access public
	 *
	 * exemplo/index.php?controle=usuario
	 *
	 */
	public $control = null;

	/**
	 * @var string: receberá o valor da acao do Admin
	 * @access public
	 *
	 * exemplo/index.php?controle=usuario&metodo=listar
	 *
	 */
	public $method = null;

	/**
	 * @var string: receberá o valor da página que será exibidada no modo public
	 * @access public
	 *
	 */
	public $pagePublic = null;


	/**
	 * Método construtor
	 * Decide se mostra o conteúdo do Admin ou public
	 * @param $content
	 */
	public function __construct( $content )
	{
		//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		# ADMIN
		if ( $content == 'admin' ) {

			# Obtém os valores do this->control e do this->method passados pela URL
			$this->get_data_admin();

			/*
			* Se o controle não tiver sido passado define o
			* controle padrão (admin/controls/home-control.php)
			*
			* Para isso, o valor setado será 'home'
			*/
			if ( empty($this->control) ) {

				$this->control = 'home';
			}

			/*
			* Se o metodo não tiver sido passado define o
			* metodo padrão index()
			*
			* Para isso, o valor setado será 'index'
			*/
			if ( empty($this->method) ) {

				$this->method = 'index';
			}

			/*echo 'Controle: ' . $this->control . '<br>';
			echo 'Método: ' . $this->method . '<br>';*/

			//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			# Inicializa o controle e método solicitado
			$this->include_admin();
		}

		//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		# PUBLIC
		elseif ( $content == 'public' ) {

			// Obtém o valor da página que será exibida
			$this->get_data_public();

			/*
			* Se a página não tiver sido passada define a
			* página padrão (public/views/_template/index.php)
			*
			*/
			if ( !isset($this->pagePublic) || empty($this->pagePublic) ) {

				$this->pagePublic = 'index';
			}

			//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			# Inclui os arquivos da visão
			$this->include_public();

		}else{
			die("O parâmetro passado na classe App é inválido!!");
		}
	}



	/**
	 * Recupera os parâmetros da URL (via  $_GET ) do Admin
	 *
	 * Seta as propriedades
	 * $this->control, $this->method
	 *
	 * Exemplo de URL:
	 * http://www.exemplo/admin/index.php?controle=usuario&metodo=listar
	 */
	private function get_data_admin()
	{
		// Verifica se os parametros ( Controle e Metodo ) foram passados via GET
		if ( isset($_GET[ Config::get("ADMIN_GET_CONTROL") ]) && isset($_GET[ Config::get("ADMIN_GET_METHOD") ]) ) {

			# Filtra e obtem os dados
			$this->control = strtolower( WolfFilter::sanitize_string( $_GET[ Config::get("ADMIN_GET_CONTROL") ] ) );
			$this->method  = strtolower( WolfFilter::sanitize_string( $_GET[ Config::get("ADMIN_GET_METHOD") ] ) );
		}
	}


	/**
	 * Recupera o GET do index principal da aplicação ( public )
	 *
	 * Seta a propriedde $this->pagePublic
	 */
	private function get_data_public()
	{
		// Verifica se o foi passado via GET | 'page' esta configurado no .htaccess do public
		if ( !empty($_GET['page']) ) {

			# Filtra e obtem o valor
			$this->pagePublic = strtolower( WolfFilter::sanitize_string( $_GET['page'] ) );
		}
	}


	/*
	|--------------------------------------------------------------------------
	| INCLUDES
	|--------------------------------------------------------------------------
	|
	*/
	private function include_admin()
	{
		if (file_exists( Config::get("ABSPATH_CONTROLS_ADMIN") .$this->control. '-control-admin.php')) {

		    # Inclui o controle na página
		    require_once Config::get("ABSPATH_CONTROLS_ADMIN") .$this->control. '-control-admin.php';

		    /*
		    * Tratando o dado recebido no $this->control
		    *
		    * Converte para caracteres minusculos
		    * Concatena com a strig "Control"
		    * Converte o 1º caractere para maiusculo
		    *
		    * Padronização do nome da classe: "NomedaclasseControlAdmin"
			*/
		    $this->control = ucwords($this->control . 'ControlAdmin');

		    # ^(*.*)> Sucesso - Instancia o "Objeto controle" que foi requisitado <(*.*)^
			$this->control = new $this->control; // Relação de composição



		    /*
		    * Verifica se o método solicitado existe no objeto $this->controle
		    */
		    if ( method_exists( $this->control, $this->method ) ) {

		        # ^(*.*)> Sucesso - Executa o método requisitado <(*.*)^
		        $this->control->{$this->method}();

		    }else {

		        # Erro - Se não existir, inicializa o método erro404 do controle principal
		        $this->control->erro404();

		        # Salva o log de erro
		        if ( Config::get("LOGGER") === true ) {
		            WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/404/', 'Erro 404: ' . get_url_actual(), 'html' );
		        }
		    }
		}

		// O controle solicitado não existe
		else {

		    require_once Config::get("ABSPATH_CONTROLS_ADMIN") . 'home-control-admin.php';

		    # Instacia o controle HomeControl
		    $this->control = new HomeControlAdmin;

		    # Chama o metodo erro404 do controle principal
		    $this->control->erro404();

		    # Salva o log de erro
		    if ( Config::get("LOGGER") === true ) {
		        WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/404/', 'Erro 404: ' . get_url_actual(), 'html' );
		    }
		}
	}

	private function include_public()
	{
		// Verifica se a página requisitada existe
		if ( file_exists( Config::get("ABSPATH_PUBLIC_TEMPLATE") . $this->pagePublic. '.php') ) {

		    # Caso sim, a página será incluída
		    require_once Config::get("ABSPATH_PUBLIC_TEMPLATE") . $this->pagePublic. '.php';
		}
		else{

			# Salva o log de erro
		    if ( Config::get("LOGGER") === true ) {
		    	WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/404/',  'Public: ' . get_url_actual(), 'html' );
		    }

			// Caso não será incluído a página de erro 404
			require_once Config::get("ABSPATH_PUBLIC_TEMPLATE") . '404.php';
		}
	}
}