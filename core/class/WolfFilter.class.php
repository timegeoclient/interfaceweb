﻿<?php
/**
 * This class comes to filter the data received by it
 * Types: SANATIZE e VALIDATE
 *
 * @author Françuel Soares <fs.francuelsoares@gmail.com>
 * @copyright Copyright (c) 2016, Françuel Soares <fs.francuelsoares@gmail.com>
 * @license http://opensource.org/licenses/ISC ISC License (ISC)
 * @link https://github.com/FrancuelSoares
 * @since 2016-01-01
 * @version 1.0
 */
final class WolfFilter
{
    /**
     * constructor
     *
     * @access private
     */
    private function __construct() {}


    /*
    |--------------------------------------------------------------------------
    | SANATIZE
    |--------------------------------------------------------------------------
    |
    */

	/**
     * Remove all characters other than numerical
     *
     * @param $value
     * @return int
     */
    public static function sanitize_int($value)
    {
        return filter_var(trim($value), FILTER_SANITIZE_NUMBER_INT);
    }


    /**
     * Remove unwanted character scripts
     *
     * @param $value
     * @return string
     */
    public static function sanitize_string($value)
    {
        return filter_var(trim($value), FILTER_SANITIZE_STRING);
    }


    /**
     * Escapes html commands
     *
     * @param $value
     * @return string
     */
    public static function sanitize_chars($value)
    {
        return filter_var(trim($value), FILTER_SANITIZE_SPECIAL_CHARS);
    }


    /**
     * Escapes tags
     *
     * @param $value
     * @return string
     */
    public static function sanitize_tags($value)
    {
        return strip_tags($value, '<h1><h2><h3><h4><h5><h6><p><img><div><iframe><blockquote><code><a><ul><ol><li>');
    }


    /**
     * Remove unwanted characters in the URL
     *
     * @param $value
     * @return string
     */
    public static function sanitize_url($value)
    {
        return filter_var(trim($value), FILTER_SANITIZE_URL);
    }


    /**
     * Remove unwanted characters email
     *
     * @param $value
     * @return string
     */
    public static function sanitize_email($value)
    {
        return filter_var(trim($value), FILTER_SANITIZE_EMAIL);
    }


    /**
     * Remove characters from unwanted scripts from an array
     *
     * @param $value
     * @param $type_filter
     * @return $value
     */
    public static function sanitize_array(array $value, $type_filter)
    {
        if (is_array($value)) {

            switch ($type_filter) {
                case 'int':
                    foreach ($value as $key => $val) {

                        $value[$key] = WolfFilter::sanitize_int($val);
                    }
                    break;

                case 'string':
                    foreach ($value as $key => $val) {

                        $value[$key] = WolfFilter::sanitize_string($val);
                    }
                    break;

                case 'chars':
                    foreach ($value as $key => $val) {

                        $value[$key] = WolfFilter::sanitize_chars($val);
                    }
                    break;

                case 'tags':
                    foreach ($value as $key => $val) {

                        $value[$key] = WolfFilter::sanitize_tags($val);
                    }
                    break;

                case 'url':
                    foreach ($value as $key => $val) {

                        $value[$key] = WolfFilter::sanitize_url($val);
                    }
                    break;

                case 'email':
                    foreach ($value as $key => $val) {

                        $value[$key] = WolfFilter::sanitize_email($val);
                    }
                    break;

                default:
                    throw new Exception("Error Processing Request!");
            }

            return $value;
        }else{
            return array();
        }
    }


    /*
    |--------------------------------------------------------------------------
    | VALIDATE
    |--------------------------------------------------------------------------
    |
    */

    /**
     * Validates the value with empty (empty string, false, integer = 0, string = 0, null, empty array)
     *
     * @param $value
     * @return boolean
     */
    public static function full_required($value)
    {
        return !empty($value) ? true : false;
    }

    /**
     * Validates that the value of this setted
     *
     * @param $value
     * @return boolean
     */
    public static function required($value)
    {
       return isset($value) ? true : false;
    }


    /**
     * Validates that is of type string
     *
     * @param $value
     * @return boolean
     */
    public static function validate_string($value)
    {
        return is_string($value) ? true : false;
    }

    /**
     * Validates that is of type numeric
     *
     * @param $value
     * @return boolean
     */
    public static function validate_numeric($value)
    {
        return is_numeric($value) ? true : false;
    }

    /**
     * Validates that is of type int
     *
     * @param $value
     * @return boolean
     */
    public static function validate_int($value)
    {
        return is_int($value) ? true : false;
    }

    /**
     * Validates that is of type float
     *
     * @param $value
     * @return boolean
     */
    public static function validate_float($value)
    {
        return is_float($value) ? true : false;
    }

    /**
     * Validates that is of type array
     *
     * @param $value
     * @return boolean
     */
    public static function validate_array($value)
    {
        return is_array($value) ? true : false;
    }

    /**
     * Validates that is of type bool
     *
     * @param $value
     * @return boolean
     */
    public static function validate_bool($value)
    {
        return is_bool($value) ? true : false;
    }

    /**
     * Validates that is of type null
     *
     * @param $value
     * @return boolean
     */
    public static function validate_null($value)
    {
        return is_null($value) ? true : false;
    }


    /**
     * Validate that contains only characters alphas
     *
     * @param $value
     * @return boolean
     */
    public static function validate_alfa($value)
    {
        return (preg_match('/^([a-zÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ])+$/i', $value) == true) ? true : false;
    }

    /**
     * Validate that contains only characters alphanumeric
     *
     * @param $value
     * @return boolean
     */
    public static function validate_alphanumeric($value)
    {
        return (preg_match('/^([a-z0-9ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ])+$/i', $value) == true) ? true : false;
    }

    /**
     * Validate that contains only character alpha and numeric, accepting "-" and "_"
     *
     * @param $value
     * @return boolean
     */
    public static function validate_alphanumeric_dash($value)
    {
        return (preg_match('/^([a-z0-9ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ_-])+$/i', $value) == true) ? true : false;
    }

    /**
     * Validate that contains only character alpha, accepting "-" and "_"
     *
     * @param $value
     * @return boolean
     */
    public static function validate_alpha_dash($value)
    {
        return (preg_match('/^([a-zÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ_-])+$/i', $value) == true) ? true : false;
    }

    /**
     * Validate that contains only character alpha and numeric, accepting spaces
     *
     * @param $value
     * @return boolean
     */
    public static function validate_alphanumeric_space($value)
    {
        return (preg_match("/^([a-z0-9ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ\s])+$/i", $value) == true) ? true : false;
    }

    /**
     * Validate that contains only character alpha, accepting spaces
     *
     * @param $value
     * @return boolean
     */
    public static function validate_alpha_space($value)
    {
        return (preg_match("/^([a-zÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ\s])+$/i", $value) == true) ? true : false;
    }


    /**
     * Validates whether an IP address
     *
     * @param $value
     * @return boolean
     */
    public static function validate_ip($value)
    {
        return (filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE)) ? true : false;
    }

    /**
     * Validates if a URL
     *
     * @param $value
     * @return boolean
     */
    public static function validate_url($value)
    {
        return (filter_var($value, FILTER_VALIDATE_URL)) ? true : false;
    }

    /**
     * Validates that is an email
     *
     * @param $value
     * @return boolean
     */
    public static function validate_email($value)
    {
        return (filter_var($value, FILTER_VALIDATE_EMAIL)) ? true : false;
    }



    /**
     * Validate the maximum length
     *
     * @param $value
     * @return boolean
     */
    public static function validate_max($value, $length)
    {
        return (strlen($value) <= $length) ? true : false;
    }

    /**
     * Validate the minimum length
     *
     * @param $value
     * @return boolean
     */
    public static function validate_min($value, $length)
    {
        return (strlen($value) >= $length) ? true : false;
    }
}