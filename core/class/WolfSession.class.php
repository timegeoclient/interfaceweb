<?php
/**
 * Classe para trabalhar com sessões de forma segura
 *
 * @author Françuel Soares <fs.francuelsoares@gmail.com>
 * @copyright Copyright (c) 2016, Françuel Soares <fs.francuelsoares@gmail.com>
 * @license http://opensource.org/licenses/ISC ISC License (ISC)
 * @link https://github.com/FrancuelSoares
 * @since 2016-01-01
 * @version 1.0
 */
final class WolfSession {

	/**
     * constructor
     *
     * @access private
     */
    private function __construct() {}

    /**
     * Inicializa uma sessão
     */
    public static function init()
    {

    	$session_id = session_id();

    	// Se não existir uma sessão ativa ela será inicializada
        if ( empty($session_id) ) {

	        // Bloqueia acesso a sessão atravês de javascript
	        $http_only = true;

	        // Froça as sessões usarem apenas cookies
	        if (ini_set('session.use_only_cookies', 1) == false) {

	            # Template de serviço indisponível
	            # require_once ABSPATH . 'includes/_templates/page-errors/504.php';

	            # Exibe o erro
	            if ( DEBUG_GLOBAL === true ) {
	                die("Aconteceu um erro durante a inicialização da sessão!!");
	            }

	            exit();
	        }

	        // Obtém os cookies atuais como parâmetros
	        $cookie_params = session_get_cookie_params();
	        session_set_cookie_params($cookie_params["lifetime"], $cookie_params["path"], Config::get("COOKIE_DOMAIN"), Config::get('HTTPS'), $http_only);

	        // Definindo um nome personalizado para a sessão
	        session_name( Config::get("NAME_SESSION") );

	        // Inicializa a sessão
	        session_start();

	        // Atualiza o id da sessão atual
	        session_regenerate_id();
        }
    }

    /**
     * Seta uma nova sessão
     *
     * @param $key
     * @param $value
     *
     */
    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Recupera o valor de uma sessão
     *
     * @param  $key
     * @return mixed
     *
     */
    public static function get($key)
    {
        return array_key_exists($key, $_SESSION)? $_SESSION[$key]: null;
    }


    /**
     * Checa se o valor recebido é igual ao da sessão em questão
     *
     * @param $session, $value
     */
    public static function check_equal( $key, $value )
    {
        return ( isset($_SESSION) && array_key_exists($key, $_SESSION) && $_SESSION[$key] == $value ) ? true : false;
    }


    /**
     * Destrói uma sessão específica
     *
     * @param  $key
     */
    public static function destroy_only($key)
    {

        if(array_key_exists($key, $_SESSION)){

            $_SESSION[$key] = null;
            unset($_SESSION[$key]);
        }
    }

    /**
     * Destrói todas as sessões
     *
     */
    public static function destroy_all()
    {

        if ( isset($_SESSION) ) {

            // Zera os valores da sessão
            $_SESSION = array();

            // Obtém os cookies atuais como parâmetros
            $cookieParams = session_get_cookie_params();

            // Deleta o cookie atual
            setcookie(session_name(),'', time() - 42000, $cookieParams["path"], $cookieParams["domain"], $cookieParams["secure"], $cookieParams["httponly"]);

            // Destrói a sessão
            session_destroy();
        }
    }
}