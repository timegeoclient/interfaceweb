<?php
/**
 * Classe que grava logs de erros ou sucessos em arquivos .html
 *
 * @author Françuel Soares <fs.francuelsoares@gmail.com>
 * @copyright Copyright (c) 2016, Françuel Soares <fs.francuelsoares@gmail.com>
 * @license http://opensource.org/licenses/ISC ISC License (ISC)
 * @link https://github.com/FrancuelSoares
 * @since 2016-01-01
 * @version 1.0
 */
final class WolfLogger
{
    /**
     * @var string: armazena o caminho de onde deve ser salvo o $file
     * @access private
     *
     */
    private static $path;

    /**
     * @var string: armazena o nome do arquivo
     * @access private
     *
     */
    private static $file;

    /**
     * @var string: armazena as extensões de arquivos permitidos
     * @access private
     *
     */
    private static $type_file = array('html', 'txt');

    /**
     * @var string: armazena o conteudo da mensagem
     * @access private
     *
     */
    private static $message;


    /**
     * Metodo construtor
     * @param $path
     * @param $message
     */
    private function __construct( $path, $message, $ext )
    {

        // Checa se a extensão é válida
        if (!in_array($ext, self::$type_file)) {
            die("A extensão {$ext} não é permitida para arquivos de logs!!");
        }

        // Guarda o Caminho
        self::$path = $path;

        // Guarda a mensagem
        self::$message = $message;

        // Nome = dia-mes-ano do log
        self::$file = date("d-m-Y");

        //Concatena o nome com extensão
        self::$file .= '.' . $ext;
    }



    /**
     * Escreve uma mensagem no arquivo de Log
     */
    public static function write( $path, $message, $ext )
    {

        # Instacia a classe e seta suas propriedades
        new WolfLogger( $path, $message, $ext );

        // Se o caminho ainda não existir, ele será criado
        if( !is_dir( self::$path ) )
        {
            mkdir(self::$path, 0755);
        }

        $file = self::$path . self::$file;

        // Abrindo o arquivo
        if ( $open = fopen( $file, "a" ) ) {

            $time = date("d-m-Y H:i:s");
            $ip   = get_ip();
            $url  = get_url_actual();

            // Monta a string que será salva dependendo do tipo do arquivo
            switch ($ext) {
                case 'html':
                    $content  = "<p>\n";
                    $content .= "<b>Data: <i>{$time}</i> </b> <br> \n";
                    $content .= "<b>IP: <i>{$ip}</i> </b> <br> \n";
                    $content .= "<b>URL: <i>{$url}</i> </b> <br> \n";
                    $content .= "<b>MENSAGEM: </b><i> ".self::$message."</i> <br> \n";
                    $content .= "</p> <hr>\n";
                    break;
                case 'txt':
                    $content  = "Data: {$time}\r\n";
                    $content .= "IP: {$ip}\r\n";
                    $content .= "URL: {$url}\r\n";
                    $content .= "MENSAGEM: ".self::$message."\r\n";
                    $content .= "----------------------------------------------------------------------------------------\r\n";
                    break;
            }

            // Escrevendo no arquivo
            fwrite( $open, $content );

            // Fechando o arquivo
            fclose( $open );

            # Sucesso :D
            return true;

        }else{

            # Erro :X
            return false;
        }
    }
}