<?php
/**
 * Classe para segurança de formulários
 * - AntiSpam
 * - Gera e checa Tokens de segurança para Formulários
 *
 * @author Françuel Soares <fs.francuelsoares@gmail.com>
 * @copyright Copyright (c) 2016, Françuel Soares <fs.francuelsoares@gmail.com>
 * @license http://opensource.org/licenses/ISC ISC License (ISC)
 * @link https://github.com/FrancuelSoares
 * @since 2016-01-01
 * @version 1.0
 */
final class WolfFormSecurity
{


    /**
     * @var $token armazena o token
     * @access protected static
     *
     */
    protected static $token;


    /**
     * constructor
     *
     * @access private
     */
    private function __construct() {}



    /**
     * Gera um token de segurança
     */
    public static function generate_token()
    {
        // Garante uma única instância. Se não existe um Token, criamos um novo
        if ( !self::$token )
        {
            // Verifica se já foi iniciado uma sessão durante a aplicação
            if ( isset($_SESSION) ) {

                // Gerando e Criptografando o $token
                self::$token = sha1(uniqid(mt_rand(), true));

                // Salvando o $token em uma sessão
                WolfSession::set('tokenForm', self::$token);

            }else{

                # Exibe o erro
                if ( DEBUG_GLOBAL === true ) {
                    echo "<p style='color:red;'>A criação do Token de segurança não pode ser efetuada! Ainda não foi startado uma session.<p>";
                }

                exit();
            }
        }
    }

    /**
     * Obtem o valor de um Token gerado
     * @return $this-token
     */
    public static function get_token()
    {
        if ( self::$token )
        {
            return $_SESSION['tokenForm'];
        }
    }

    /**
     * Checa e valida o Token
     * @param $data_token = token recebido pelo formulario
     * @param $session_token = token armazenado na session
     * @return bool
     */
    public static function check_token($data_token, $session_token)
    {
        if ( $data_token == $session_token ) {

            # Sucesso :D
            return true;

        }else{

            # Erro :X
            WolfSession::destroy_only('tokenForm');
            return false;
        }
    }


    /**
     * Detecta tentivas inválidas na submissão de formulários
     */
    public static function check_spam_form()
    {
        # Captura dos dados
        $sEmail  = isset( $_POST['sEmail']) ? $_POST['sEmail'] : NULL; // Email
        $sHora   = isset( $_POST['sHora']) ? $_POST['sHora'] : NULL; // Email
        $sToken  = isset( $_POST['sToken']) ? $_POST['sToken'] : NULL; // Email

        /*echo 'Hora no Form: '.$sHora.'<br>';
        echo 'Hora Atual: '.time().'<br>';
        echo '<br>';
        echo 'Tokem no form: '.$sToken.'<br>';
        echo 'Tokem na sessão: '.$_SESSION['tokenForm'].'<br>';*/

        if ( ($sEmail == '') && ($sHora < (time() + 1)) && WolfFormSecurity::check_token( $sToken, $_SESSION['tokenForm'] ) )
        {
            # Ok :D
            return true;
        }else{
            # Opa! tem algo de errado! O_o
            return false;
        }
    }
}