<?php

/**
 * Classe Padrão para todos os Modelos do Admin
 *
 * @version 1.0
 */
abstract class MainModelAdmin
{

	/**
	 * @var array: armazena mensagen de sucesso
	 * @access public
	 *
	 */
	public $msgSuccess = array();

	/**
	 * @var array: armazena mensagen de erros
	 * @access public
	 *
	 */
	public $msgError = array();
}