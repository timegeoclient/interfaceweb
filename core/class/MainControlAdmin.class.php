<?php

/**
 * Classe Padrão para todos os controles do Admin
 *
 * @version 1.0
 */
abstract class MainControlAdmin {

	/**
	 * @var object: armazena uma instacia dos modelos (Relação de composição)
	 * @access public
	 *
	 */
	public $model;

	/**
	 * @var array: armazena os arquivos para a visao funcionar
	 * @access protected
	 *
	 */
	protected $files = array('header.php', 'wrapper.php', 'footer.php');

	/**
	 * @var string: visao que será inclusa dependendo do controle e metodo escolhido
	 * @access protected
	 *
	 */
	protected $view = null;

	/**
	* @var array: dados para serem recuperados e impressos dentro de uma visao.
	* @access protected
    *
    * Exe: titulo, description, keywords, etc
    */
    protected $data_view = array( 'author' 		=> 'GeoClient',
    							  'keywords' 	=> 'palavra-chave'
    							  );


	/*
	* Retorna todos os arquivos básicos
	* que se juntam com a visao, formando a página completa
	*/
	protected function get_files()
	{
		return $this->files;
	}



	/**
	 * Salva qual a visao que será inclusa
	 * @param $view
	 */
	protected function set_view($view)
	{
		$this->view = $view;
	}




	/**
	 * Incluiu o arquivo da visao selecionada pelo controle
	 */
	public function get_view()
	{
		$file = Config::get("ABSPATH_VIEWS_ADMIN") . $this->view;

		// Verifica se o arquivo da visao existe na pasta views
		if ( file_exists( $file ) ) {

			# Retorna o link do arquivo :D
			return $file;

		}else {

			# Seta uma nova visão (erro404)
		    $this->set_view('error/404-view-admin.php');

		    $file = Config::get("ABSPATH_VIEWS_ADMIN") .$this->view;

		    # Retorna o link do arquivo :D
			return $file;
		}
	}


	/**
	 * Inclui todos os arquivos básicos que formam a visão
	 */
	protected function include_files()
	{
		foreach ( $this->get_files() as $file ) {

			if ( file_exists( Config::get("ABSPATH_VIEWS_ADMIN") . '_template/' . $file ) )
		    	require_once Config::get("ABSPATH_VIEWS_ADMIN") . '_template/' . $file;
		    else
		    	die("Um arquivo do template de nome '$file' não pode ser encontradao!!");
		}
	}


	/**
	 * Salva dados que serão recuperados pela visao
	 * Devem serem passados como array
	 * Sempre será salvo na ultima posição do array
     * @param $array
	 */
	protected function set_data_view(array $array)
	{
		# Mescla o array já existente com o novo :D
		$this->data_view = array_merge($this->data_view, $array);
	}

	/*
	* Recupera um dado específico do array com dados da visao
	*/
	public function get_data_view($key)
	{
		// Retorna o valor :D
		return $this->data_view[$key];
	}



	/**
	 * Inicializado caso o método solicitado não exista
	 * Carrega a página (visao) "admin/views/error/404-view-admin.php"
	 */
	public function erro404() {

		# Essa visão não possui um modelo

		/**
		 * Define dados que serão recuperados na página
		 */
		$this->set_data_view( $array = array( 'title'			=> Config::get("NAME_PROJECT") . ' | Erro 404',
    										  'description' 	=> 'Essa página não pode ser encontrada!'
											) );

		/**
		 * Definindo qual será a visao exibida
		 * Sem "/" no inicio da strig, e por o .php no final
		 */
		$this->set_view('error/home/404-view-admin.php');

		/**
		 * Inclui os arquivos do template que formam a base da visão
		 */
		$this->include_files();
	}



	/**
	 * Retorna as mensagens de erro do modelo
	 * @return array
	 */
	public function get_msgError()
	{
		return $this->model->msgError;
	}

	/**
	 * Retorna as mensagens de sucesso do modelo
	 * @return array
	 */
	public function get_msgSuccess()
	{
		return $this->model->msgSuccess;
	}
}