<?php
/**
 * Class para usar o Google Maps Geocoding API
 *
 * Geocodificação: é o processo de conversão de endereços (como "1600 Amphitheatre Parkway, Mountain View, CA")
 * em coordenadas geográficas (como latitude 37.423021 e longitude -122.083739) que podem ser usadas para
 * inserir marcadores em um mapa ou posicionar o mapa.
 *
 * Gratuito até 2.500 solicitações por dia!
 *
 * @version 1.0
 */
final class Geocoding extends GoogleMapsAPI {

    /**
     * @var Armazena a latitude e longitude obtida
     * @access protected
     */
    private $result = array( 'lat' => '', 'lng' => '' );


	/**
     * Constructor
     * @access public
     */
    public function __construct(){}



    /**
     * Solicita a Latitude e a Longitude
     *
     */
    private function request_lat_lng()
    {
        $request = @file_get_contents( $this->url );
        $request = json_decode($request, true);

        // Se não retornou resultados. Endereço pode ser inexistente
        if ( $request['status'] == 'ZERO_RESULTS' || empty($request) ) {
            $this->msgErrorGM = array_replace( $this->msgErrorGM , array( 0 => 'Nenhum resultado foi encontrado!' ) );
            return false;

        }elseif ( $request['status'] == 'OVER_QUERY_LIMIT' ) {
            $this->msgErrorGM = array_replace( $this->msgErrorGM , array( 1 => 'A cota de solicitações da API foi ultrapasada!' ) );
            return false;

        }elseif ( $request['status'] == 'REQUEST_DENIED' ) {
            $this->msgErrorGM = array_replace( $this->msgErrorGM , array( 2 => 'A solicitação foi negada!') );
            return false;

        }elseif ( $request['status'] == 'INVALID_REQUEST' ) {
            $this->msgErrorGM = array_replace( $this->msgErrorGM , array( 3 => 'Algum parâmetro da consuta (address, components ou latlng) está ausente!' ) );
            return false;

        }elseif ( $request['status'] == 'UNKNOWN_ERROR' ) {
            $this->msgErrorGM = array_replace( $this->msgErrorGM , array( 4 => 'A solicitação não foi processada devido a um erro de servidor!' ) );
            return false;

        }elseif ( $request['status'] == 'OK' ){
            $this->result['lat'] = ( isset($request['results'][0]['geometry']['location']['lat']) ) ? $request['results'][0]['geometry']['location']['lat'] : null;
            $this->result['lng'] = ( isset($request['results'][0]['geometry']['location']['lng']) ) ? $request['results'][0]['geometry']['location']['lng'] : null;

            # Sucesso :D
            return true;
        }
    }


    /**
     * Reformula a string do endereço para o valor certo
     * "+"" no lugar de espaços
     * @param $address
     */
    private function sanatize_address( $address )
    {
        return str_replace( " ", "+", $address );
    }



    /**
     * Retorna o valor da latidude e longitude para a aplicação
     *
     * @param $address = endereço que se quer a Lat e Lgn
     */
    public function get_lat_lng( $address )
    {
        $this->address = $this->sanatize_address( $address );
        $this->url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$this->address.'&key='.self::KEY_API;
        //echo $this->url;

        # Executa a consulta
        $this->request_lat_lng();

        # Retorna o resultado
        return $this->result;
    }
}