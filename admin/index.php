<?php
/**
 * Pattern: Front Controller
 *
 * Esse index.php atua como um script centralizador de toda a parte ADMIN da aplicação
 *
 * Ao executar a classe App.class.php,
 * Decidirá qual página será exibida
 *
 * @version 1.0
 */

/*
|--------------------------------------------------------------------------
| Arquivos necessarios para a aplicação funcionar
|  - Arquivo de configuração
|  - Funções principais da aplicação  ( Autoload, etc )
|--------------------------------------------------------------------------
|
*/
if (!file_exists('../config.php'))
	die("Error!! The configuration file can not be found.");

require_once '../config.php'; // Configurações Básicas
require_once ABSPATH.'core/functions/Main.php'; // Funções


/*
|--------------------------------------------------------------------------
| Verifica se o debug para o Admin esta habilitado
|--------------------------------------------------------------------------
|
*/
if ( Config::get('ADMIN_DEBUG') === false ) {

	# Esconde todos os erros
	error_reporting(0);
	ini_set("display_errors", 0);

} else {

	# Mostra todos os erros
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}


/*
|--------------------------------------------------------------------------
| Inicializa uma sessão
|--------------------------------------------------------------------------
|
*/
WolfSession::init();


/*
|--------------------------------------------------------------------------
| Verifica se o Admin esta logado
|--------------------------------------------------------------------------
|
*/
require_once Config::get("ABSPATH_CONTROLS_ADMIN") . 'login-control-admin.php'; // Inclui o controle de Login
$login = new LoginControlAdmin;

if ( $login->logged() == false ) {
    # Usuário não esta logado
    header("Location: " . URL_HOME . "admin/login.php");
    exit();
}


/*
|--------------------------------------------------------------------------
| Verifica se aplicação encontra-se em manutenção
|--------------------------------------------------------------------------
|
*/
if ( Config::get('ADMIN_MAINTENANCE') === true ) {

	#require_once ABSPATH . '/includes/_templates/maintenance/public.php';
	die("A aplicação ADMIN encontra-se em manutenção!");
	exit();
}


/*
|--------------------------------------------------------------------------
| Executa a aplicação
|--------------------------------------------------------------------------
|
*/
/*
$data_email = array(
    'EMAIL_USE_SMTP' => true,
    'FromEmail'      => 'appfran10@gamil.com',
    'FromName'       => 'King Wolf',
    'AddressEmail'   => 'fs.francuelsoares@gmail.com',
    'AddressName'    => 'Françuel Soares',
    'subject'        => 'Validação de Admin',
    'messageBody'    => '<hr><br><h1>Usuáurio validado com sucesso!</h1><br><hr>',
    'messageAltBody' => 'Usuáurio validado com sucesso!'
    );

if ( WolfEmail::send_email( $data_email ) )
	echo 'Mensagem enviada com sucesso!!';
*/
/*
echo 'Sessões';
echo '<pre>';
print_r($_SESSION);
echo '</pre><hr>';

echo 'GET';
echo '<pre>';
(isset($_GET)) ? print_r($_GET) : $_GET = array();
echo '</pre>';*/

(new App('admin'));