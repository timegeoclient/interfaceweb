<?php

/**
 * Controle que gerencia o acesso do Admin na aplicação
 * Usa composição para relacionar com a classe LoginAdminModel
 *
 * @version 1.0
 */
final class LoginControlAdmin extends MainControlAdmin
{

	/*
	* Método construtor
	* Ao ser instanciado:
	*
	* Verifica qual operção será executada
	*
	*/
	public function __construct()
	{
		//-=-=-=-=-=-=-=-=-=-=-=-
		# Inicializa o Modelo de Login
		require_once Config::get("ABSPATH_MODELS_ADMIN") . 'login-model-admin.php'; // Arquivo
		$this->model = new LoginModelAdmin; // Relação de Composição



		//-=-=-=-=-=-=-=-=-=-=-=-
		# Ações possíveis
		//-=-=-=-=-=-=-=-=-=-=-=-

		// Se o botão Entrar do formulário de login for acionado
		if ( isset($_POST['btnEntrar']) ) {

			# Inicializa o método para validar os dados
			if ( $this->model->log_in() ) {
				# Sucesso :D
				header("Location: " . URL_HOME . "admin/");
				exit();
			}
		}
	}


	/**
	 * Verifica se o usuário esta logado
	 * @return bool
	 */
	public function logged()
	{
		# Inicializa o método admin_logged_on da classe LoginAdminModel
		if ( $this->model->logged_on() ) {
			# Sucesso :D
			return true;
		}else{
			# Erro :X
			return false;
		}
	}
}