<?php
/**
 * Controle de Importação
 *
 * @version 1.0
*/
final class ImportarControlAdmin extends MainControlAdmin
{

	/*
	 * Método construtor
	 */
	public function __construct()
	{
		//-=-=-=-=-=-=-=-=-=-=-=-
		# Inicializa o modelo
		require_once Config::get("ABSPATH_MODELS_ADMIN") . 'importar-model-admin.php'; // Arquivo
		$this->model = new ImportarModelAdmin; // Relação de Composição

		/*
	    |--------------------------------------------------------------------------
	    | Ações possíveis
	    |--------------------------------------------------------------------------
	    |
	    */

		// Se o botão de importar clientes for acionado
		if ( isset($_POST['btnImportarCli']) ) {

			$this->model->import_clients();
		}
	}


	public function index()
	{
		// Método index não existirá nesse controle
	}



	/*
    |--------------------------------------------------------------------------
    | Funçoes Primárias
    |--------------------------------------------------------------------------
    |
    */

	/*
	* Importação para clientes
	*/
	public function clientes() {

		/**
		 * Define dados que serão recuperados na página
		 */
		parent::set_data_view( $array = array(  'title'		=> Config::get("NAME_PROJECT") . ' | Importar Clientes',
    										 	'description' 	=> 'Importção de clientes'
											) );

		/**
		 * Definindo qual será a visao exibida
		 * Sem "/" no inicio da strig, e por o .php no final
		 */
		parent::set_view('importar/importar-view-clientes.php');

		/**
		 * Inclui os arquivos do template que formam a base da visão
		 */
		parent::include_files();
	}
}