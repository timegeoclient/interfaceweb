<?php
/**
 * Controle do Colaborador
 *
 * @version 1.0
*/
final class ColaboradorControlAdmin extends MainControlAdmin
{

	/*
	 * Método construtor
	 */
	public function __construct()
	{
		//-=-=-=-=-=-=-=-=-=-=-=-
		# Inicializa o modelo
		require_once Config::get("ABSPATH_MODELS_ADMIN") . 'colaborador-model-admin.php'; // Arquivo
		$this->model = new ColaboradorModelAdmin; // Relação de Composição

		/*
	    |--------------------------------------------------------------------------
	    | Ações possíveis
	    |--------------------------------------------------------------------------
	    |
	    */

		// Se o botão de cadastro for acionado
		if ( isset($_POST['btnCadastrar']) ) {

			$this->model->register_collaborator();
		}
	}


	public function index()
	{
		// Método index não existirá nesse controle
	}



	/*
    |--------------------------------------------------------------------------
    | Funçoes Primárias
    |--------------------------------------------------------------------------
    |
    */

	/*
	* Método Cadastrar Admin
	*/
	public function cadastrar() {

		/**
		 * Define dados que serão recuperados na página
		 */
		parent::set_data_view( $array = array(  'title'		=> Config::get("NAME_PROJECT") . ' | Cadastrar Colaborador',
    										 	'description' 	=> 'Cadastro de Colaborador'
											) );

		/**
		 * Definindo qual será a visao exibida
		 * Sem "/" no inicio da strig, e por o .php no final
		 */
		parent::set_view('colaborador/colaborador-view-cadastrar.php');

		/**
		 * Inclui os arquivos do template que formam a base da visão
		 */
		parent::include_files();
	}
}