<?php

/**
 * HomeControl - Controle inicial da aplicação Admin
 *
 * Instaciado caso não seja solicitado nenhum dos controles
 *
 * @version 1.0
*/
final class HomeControlAdmin extends MainControlAdmin
{

	/**
	 * Carrega a página (visao) padrão "admin/views/home/home-view-admin.php"
	 */
	public function index() {

		# Essa visão não possui um modelo

		/**
		 * Define dados que serão recuperados na página
		 */
		parent::set_data_view( $array = array(  'title'		=> Config::get("NAME_PROJECT") . ' | Home',
    										 	'description' 	=> 'Página inicial'
											) );

		/**
		 * Definindo qual será a visao exibida
		 * Sem "/" no inicio da strig, e por o .php no final
		 */
		parent::set_view('home/home-view-admin.php');

		/**
		 * Inclui os arquivos do template que formam a base da visão
		 */
		parent::include_files();
	}
}