<?php
/**
 * Controle do Administrador
 *
 * @version 1.0
*/
final class AdministradorControlAdmin extends MainControlAdmin
{

	/*
	 * Método construtor
	 */
	public function __construct()
	{
		//-=-=-=-=-=-=-=-=-=-=-=-
		# Inicializa o modelo
		require_once Config::get("ABSPATH_MODELS_ADMIN") . 'administrador-model-admin.php'; // Arquivo
		$this->model = new AdministradorModelAdmin; // Relação de Composição

		/*
	    |--------------------------------------------------------------------------
	    | Ações possíveis
	    |--------------------------------------------------------------------------
	    |
	    */

		// Se o botão de cadastro for acionado
		if ( isset($_POST['btnCadastrar']) ) {

			$this->model->register_admin();
		}
	}


	public function index()
	{
		// Método index não existirá nesse controle
	}



	/*
    |--------------------------------------------------------------------------
    | Funçoes Primárias
    |--------------------------------------------------------------------------
    |
    */

	/*
	* Método Cadastrar Admin
	*/
	public function cadastrar() {

		/**
		 * Define dados que serão recuperados na página
		 */
		parent::set_data_view( $array = array(  'title'		=> Config::get("NAME_PROJECT") . ' | Cadastrar Administrador',
    										 	'description' 	=> 'Cadastro de Administrador'
											) );

		/**
		 * Definindo qual será a visao exibida
		 * Sem "/" no inicio da strig, e por o .php no final
		 */
		parent::set_view('administrador/administrador-view-cadastrar.php');

		/**
		 * Inclui os arquivos do template que formam a base da visão
		 */
		parent::include_files();
	}
}