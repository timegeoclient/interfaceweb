<?php
/**
 * Desloga o usuário
 *
 * @version 1.0
 */

/*
|--------------------------------------------------------------------------
| Arquivos necessarios para a aplicação funcionar
|  - Arquivo de configuração
|  - Funções principais da aplicação  ( Autoload, etc )
|--------------------------------------------------------------------------
|
*/
if (!file_exists('../config.php'))
	die("Error!! The configuration file can not be found.");

require_once '../config.php'; // Configurações Básicas
require_once ABSPATH.'core/functions/Main.php'; // Funções


/*
|--------------------------------------------------------------------------
| Verifica se o debug para o Admin esta habilitado
|--------------------------------------------------------------------------
|
*/
if ( Config::get('ADMIN_DEBUG') === false ) {

	# Esconde todos os erros
	error_reporting(0);
	ini_set("display_errors", 0);

} else {

	# Mostra todos os erros
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}


/*
|--------------------------------------------------------------------------
| Inicializa uma sessão
|--------------------------------------------------------------------------
|
*/
WolfSession::init();


/*
|--------------------------------------------------------------------------
| Desloga o usuário
|  - Inicializa o controe de Login
|--------------------------------------------------------------------------
|
*/
require_once Config::get("ABSPATH_CONTROLS_ADMIN") . 'login-control-admin.php'; // Inclui o controle de Login
$login = new LoginControlAdmin;

$login->model->logout();

header("Location: " . URL_HOME . "admin/login.php");
exit();