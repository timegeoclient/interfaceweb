<?php if ( !defined('ABSPATH')) exit; ?>
        <div class="panel-content container-fluid">

            <div class="page-head">
                <h1>Início <small>Página Inicial</small></h1>
                <ol class="breadcrumb">
                    <li class="active">Home</li>
                </ol>
            </div>

            <div class="page-content">

                <div class="row">

                    <!--Block-->
                    <div class="col-sm-6 col-md-6">
                        <div class="page-block">
                            <div class="page-block_header">
                                <h3>Title Block</h3>
                            </div>
                            <div class="page-block_content">
                                <div class="alert alert-warning alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Warning!</strong> Better check yourself, you're not looking too good.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Block-->

                    <!--Block-->
                    <div class="col-sm-6 col-md-6">
                        <div class="page-block">
                            <div class="page-block_header">
                                <h3>Title Block</h3>
                            </div>
                            <div class="page-block_content">
                                <div class="alert alert-warning alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Warning!</strong> Better check yourself, you're not looking too good.
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End Block-->
                </div>

            </div><!--page-content-->
        </div><!--panel-content-->