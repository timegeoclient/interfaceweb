<?php if ( !defined('ABSPATH')) exit; ?>
        <div class="panel-content container-fluid">

            <div class="page-head">
                <h1>Colaborador <small>Cadastro</small></h1>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">Colaborador</a></li>
                    <li class="active">Cadastrar</li>
                </ol>
            </div>

            <div class="page-content">

                <div class="row">

                    <!--Block-->
                    <div class="col-sm-12 col-md-12">
                        <div class="page-block">
                            <div class="page-block_header">
                                <h3>Formulário de cadastro</h3><hr>
                            </div>

                            <?php
                                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                // Exibe mensagens de erro
                                if ( !empty( $this->get_msgError() ) ) {

                                    foreach ( $this->get_msgError() as $value ) {
                                        echo msg_error_default($value);
                                    }
                                }

                                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                // Exibe mensagens de sucesso
                                if ( !empty( $this->get_msgSuccess() ) ) {

                                    foreach ( $this->get_msgSuccess() as $value ) {
                                        echo msg_success_default($value);
                                    }
                                }

                                /*
                                |--------------------------------------------------------------------------
                                | Gera um Token de segurança para formulários
                                |--------------------------------------------------------------------------
                                |
                                */
                                WolfFormSecurity::generate_token();
                            ?>

                            <div class="page-block_content">

                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" parsley-validate novalidate>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Nome Completo</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="txtNOMCOL" value="<?php echo (isset($_POST['txtNOMCOL'])) ? $_POST['txtNOMCOL'] : ''; ?>" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Empresa</label>
                                        <div class="col-sm-5">
                                            <select class="form-control" name="txtEMPCOL" class="form-control">
                                                <option value="">Selecione</option>
                                            <?php
                                                # Selecionando as empresa cadastradas
                                                $result = $this->model->select_all_empresas();

                                                // Listando
                                                foreach ($result as $row) :
                                            ?>
                                                <option value="<?php echo $row['id_empresa']; ?>" <?php echo ( (isset($_POST['txtEMPCOL'])) && $row['id_empresa'] == $_POST['txtEMPCOL'] ) ? 'selected' : ''; ?>><?php echo strtoupper($row['nome_empresa']); ?></option>
                                            <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-5">
                                            <input type="email" name="txtEMACOL" value="<?php echo (isset($_POST['txtEMACOL'])) ? $_POST['txtEMACOL'] : ''; ?>" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Nome de usuario</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="txtUSUCOL" value="<?php echo (isset($_POST['txtUSUCOL'])) ? $_POST['txtUSUCOL'] : ''; ?>" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Senha</label>
                                        <div class="col-sm-5">
                                            <input type="password" name="txtSENCOL" value="<?php echo (isset($_POST['txtSENCOL'])) ? $_POST['txtSENCOL'] : ''; ?>" class="form-control" parsley-minlength="8" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Confirme a senha</label>
                                        <div class="col-sm-5">
                                            <input type="password" name="txtSENCOL2" value="<?php echo (isset($_POST['txtSENCOL2'])) ? $_POST['txtSENCOL2'] : ''; ?>" class="form-control" parsley-minlength="8" required>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">MAC</label>
                                        <div class="col-sm-5">
                                            <input id="mac" type="text" name="txtMACCOL" value="<?php echo (isset($_POST['txtMACCOL'])) ? $_POST['txtMACCOL'] : ''; ?>" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Sistema Operacional</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="txtSOCOL" value="<?php echo (isset($_POST['txtSOCOL'])) ? $_POST['txtSOCOL'] : ''; ?>" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Versão do SO</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="txtVERCOL" value="<?php echo (isset($_POST['txtVERCOL'])) ? $_POST['txtVERCOL'] : ''; ?>" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Número</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="txtNUMCOL" value="<?php echo (isset($_POST['txtNUMCOL'])) ? $_POST['txtNUMCOL'] : ''; ?>" class="form-control" required>
                                        </div>
                                    </div>

                                    <input type="text" name="sEmail" value="" style="display: none;">
                                    <input type="text" name="sHora" value="<?php echo time(); ?>" style="display: none;">
                                    <input type="hidden" name="sToken" value="<?php echo WolfFormSecurity::get_token(); ?>">

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" name="btnCadastrar" class="btn btn-success">Cadastrar</button>
                                        <button type="reset" class="btn btn-default">Resetar</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--End Block-->
                </div>

            </div><!--page-content-->
        </div><!--panel-content-->