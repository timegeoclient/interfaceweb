<?php if ( !defined('ABSPATH')) exit; ?>
    <!-- WRAPPER -->
    <div id="panel-wrapper">

        <!-- SIDEBAR -->
        <?php require_once Config::get("ABSPATH_VIEWS_ADMIN") . '_template/sidebar.php'; ?>
        <!-- END SIDEBAR -->

        <!-- VIEW -->
        <?php require_once $this->get_view(); ?>
        <!-- END VIEW -->
    </div>
    <!-- END WRAPPER -->