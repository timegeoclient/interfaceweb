<?php if ( !defined('ABSPATH')) exit; ?>
        <div class="panel-sidebar">
            <div class="nav-side-menu">
                <div class="brand"><i class="glyphicon glyphicon-dashboard"></i> Painel Administrativo</div>
                <i class="glyphicon glyphicon-align-justify toggle-btn" data-toggle="collapse" data-target="#menu-lateral"></i>
                    <div class="menu-list">

                        <ul id="menu-lateral" class="menu-content collapse out">

                            <li  data-toggle="collapse" data-target="#adm" class="collapsed active">
                                <a href="#" onclick="return false;"><i class="glyphicon glyphicon glyphicon-cog"></i> Administrador <span class="arrow"></span></a>
                            </li>
                            <ul class="sub-menu collapse" id="adm">
                                <li><a href="<?php echo 'index.php?'.Config::get("ADMIN_GET_CONTROL").'=administrador&'.Config::get("ADMIN_GET_METHOD").'=cadastrar'; ?>">Cadastrar</a></li>
                            </ul>

                            <li  data-toggle="collapse" data-target="#col" class="collapsed active">
                                <a href="#" onclick="return false;"><i class="glyphicon glyphicon-user"></i> Colaborador <span class="arrow"></span></a>
                            </li>
                            <ul class="sub-menu collapse" id="col">
                                <li><a href="<?php echo 'index.php?'.Config::get("ADMIN_GET_CONTROL").'=colaborador&'.Config::get("ADMIN_GET_METHOD").'=cadastrar'; ?>">Cadastrar</a></li>
                            </ul>

                            <li  data-toggle="collapse" data-target="#import" class="collapsed active">
                                <a href="#" onclick="return false;"><i class="glyphicon glyphicon-import"></i> Importar <span class="arrow"></span></a>
                            </li>
                            <ul class="sub-menu collapse" id="import">
                                <li><a href="<?php echo 'index.php?'.Config::get("ADMIN_GET_CONTROL").'=importar&'.Config::get("ADMIN_GET_METHOD").'=clientes'; ?>">Clientes</a></li>
                            </ul>

                            <!--<li>
                                <a href="#">
                                    <i class="glyphicon glyphicon-user"></i> Users
                                </a>
                            </li>-->
                        </ul>
                 </div>
            </div>
        </div>