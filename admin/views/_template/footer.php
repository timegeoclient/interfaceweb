<?php if ( !defined('ABSPATH')) exit; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo Config::get("URI_TEMPLATE_ADMIN"); ?>js/jquery-1.12.4.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo Config::get("URI_TEMPLATE_ADMIN"); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo Config::get("URI_TEMPLATE_ADMIN"); ?>js/Main.js"></script>

	<!--Plugins-->
    <script type="text/javascript" src="<?php echo Config::get("URI_TEMPLATE_ADMIN"); ?>js/jquery.parsley/messages.pt_br.js"></script>
	<script type="text/javascript" src="<?php echo Config::get("URI_TEMPLATE_ADMIN"); ?>js/jquery.parsley/parsley.js"></script>
	<script type="text/javascript" src="<?php echo Config::get("URI_TEMPLATE_ADMIN"); ?>js/jquery.maskedinput.min.js"></script>

</body>
</html>