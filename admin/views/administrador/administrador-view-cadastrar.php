<?php if ( !defined('ABSPATH')) exit; ?>
        <div class="panel-content container-fluid">

            <div class="page-head">
                <h1>Administrador <small>Cadastro</small></h1>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">Administrador</a></li>
                    <li class="active">Cadastrar</li>
                </ol>
            </div>

            <div class="page-content">

                <div class="row">

                    <!--Block-->
                    <div class="col-sm-12 col-md-12">
                        <div class="page-block">
                            <div class="page-block_header">
                                <h3>Formulário de cadastro</h3><hr>
                            </div>

                            <?php
                                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                // Exibe mensagens de erro
                                if ( !empty( $this->get_msgError() ) ) {

                                    foreach ( $this->get_msgError() as $value ) {
                                        echo msg_error_default($value);
                                    }
                                }

                                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                // Exibe mensagens de sucesso
                                if ( !empty( $this->get_msgSuccess() ) ) {

                                    foreach ( $this->get_msgSuccess() as $value ) {
                                        echo msg_success_default($value);
                                    }
                                }

                                /*
                                |--------------------------------------------------------------------------
                                | Gera um Token de segurança para formulários
                                |--------------------------------------------------------------------------
                                |
                                */
                                WolfFormSecurity::generate_token();
                            ?>

                            <div class="page-block_content">

                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" parsley-validate novalidate>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Nome Completo</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="txtNOMADM" value="<?php echo (isset($_POST['txtNOMADM'])) ? $_POST['txtNOMADM'] : ''; ?>" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Empresa</label>
                                        <div class="col-sm-5">
                                            <select class="form-control" name="txtEMPADM" class="form-control">
                                                <option value="">Selecione</option>
                                            <?php
                                                # Selecionando as empresa cadastradas
                                                $result = $this->model->select_all_empresas();

                                                // Listando
                                                foreach ($result as $row) :
                                            ?>
                                                <option value="<?php echo $row['id_empresa']; ?>" <?php echo ( (isset($_POST['txtEMPADM'])) && $row['id_empresa'] == $_POST['txtEMPADM'] ) ? 'selected' : ''; ?>><?php echo strtoupper($row['nome_empresa']); ?></option>
                                            <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-5">
                                            <input type="email" name="txtEMAADM" value="<?php echo (isset($_POST['txtEMAADM'])) ? $_POST['txtEMAADM'] : ''; ?>" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Nome de usuario</label>
                                        <div class="col-sm-5">
                                            <input type="text" name="txtUSUADM" value="<?php echo (isset($_POST['txtUSUADM'])) ? $_POST['txtUSUADM'] : ''; ?>" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Senha</label>
                                        <div class="col-sm-5">
                                            <input type="password" name="txtSENADM" value="<?php echo (isset($_POST['txtSENADM'])) ? $_POST['txtSENADM'] : ''; ?>" class="form-control" parsley-minlength="8" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Confirme a senha</label>
                                        <div class="col-sm-5">
                                            <input type="password" name="txtSENADM2" value="<?php echo (isset($_POST['txtSENADM2'])) ? $_POST['txtSENADM2'] : ''; ?>" class="form-control" parsley-minlength="8" required>
                                        </div>
                                    </div>

                                    <input type="text" name="sEmail" value="" style="display: none;">
                                    <input type="text" name="sHora" value="<?php echo time(); ?>" style="display: none;">
                                    <input type="hidden" name="sToken" value="<?php echo WolfFormSecurity::get_token(); ?>">

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" name="btnCadastrar" class="btn btn-success">Cadastrar</button>
                                        <button type="reset" class="btn btn-default">Resetar</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--End Block-->
                </div>

            </div><!--page-content-->
        </div><!--panel-content-->