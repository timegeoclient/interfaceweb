<?php
    if ( !defined('ABSPATH')) exit;
    /*
    |--------------------------------------------------------------------------
    | Inicializa o Controle de Login do Admin
    |--------------------------------------------------------------------------
    |
    */
    $app = new LoginControlAdmin;


    /*
    |--------------------------------------------------------------------------
    | Verifica se o Admin esta logado
    |--------------------------------------------------------------------------
    |
    */
    if ( $app->logged() ) {
        # Usuário já esta logado
        header("Location: " . URL_HOME . "admin/");
        exit();
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>GeoClient - Login</title>

    <!-- Bootstrap -->
    <link href="<?php echo Config::get("URI_TEMPLATE_ADMIN"); ?>css/bootstrap.min.css" rel="stylesheet">

    <!-- MAIN CSS -->
    <link href="<?php echo Config::get("URI_TEMPLATE_ADMIN"); ?>css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="panel-login">
                    <div class="panel-login_header">
                        <h1>Página de Login</h1>
                    </div>
                    <div class="panel-login_body">

                        <?php
                            //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                            // Exibe mensagens de erro
                            if ( !empty( $app->get_msgError() ) ) {

                                foreach ( $app->get_msgError() as $value ) {
                                    echo msg_error_default($value);
                                }
                            }

                            //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                            // Exibe mensagens de sucesso
                            if ( !empty( $app->get_msgSuccess() ) ) {

                                foreach ( $app->get_msgSuccess() as $value ) {
                                    echo msg_success_default($value);
                                }
                            }

                            /*
                            |--------------------------------------------------------------------------
                            | Gera um Token de segurança para formulários
                            |--------------------------------------------------------------------------
                            |
                            */
                            WolfFormSecurity::generate_token();
                        ?>

                        <form action="" method="post" enctype="multipart/form-data">

                            <div class="form-group">
                                <label class="control-label" for="usuario">Usuário</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input type="text" name="txtNOMUSU" value="<?php echo (isset($_POST['txtNOMUSU'])) ? $_POST['txtNOMUSU'] : ''; ?>" class="form-control" id="usuario" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label" for="senha">Senha</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                    <input type="password" name="txtSENUSU" value="" class="form-control" id="senha" required>
                                </div>
                            </div>

                            <input type="text" name="sEmail" value="" style="display: none;">
                            <input type="text" name="sHora" value="<?php echo time(); ?>" style="display: none;">
                            <input type="hidden" name="sToken" value="<?php echo WolfFormSecurity::get_token(); ?>">

                            <button type="submit" name="btnEntrar" class="btn btn-success">Entrar</button>
                            <button type="reset" class="btn btn-default">Resetar</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo Config::get("URI_TEMPLATE_ADMIN"); ?>js/bootstrap.min.js"></script>

</body>
</html>