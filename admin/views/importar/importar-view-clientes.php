<?php if ( !defined('ABSPATH')) exit; ?>
        <div class="panel-content container-fluid">

            <div class="page-head">
                <h1>Importar <small>Clientes</small></h1>
                <ol class="breadcrumb">
                    <li><a href="index.php">Home</a></li>
                    <li class="active">Importar</li>
                </ol>
            </div>

            <div class="page-content">

                <div class="row">

                    <!--Block-->
                    <div class="col-sm-12 col-md-12">
                        <div class="page-block">
                            <div class="page-block_header">
                                <h3>Formulário de Importação</h3><hr>
                            </div>

                            <?php

                                /*echo '<pre>';
                                isset($_FILES['txtCSV']) ? print_r($_FILES['txtCSV']) : print_r('');
                                echo '</pre>';*/

                                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                // Exibe mensagens de erro
                                if ( !empty( $this->get_msgError() ) ) {

                                    foreach ( $this->get_msgError() as $value ) {
                                        echo msg_error_default($value);
                                    }
                                }

                                //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                                // Exibe mensagens de sucesso
                                if ( !empty( $this->get_msgSuccess() ) ) {

                                    foreach ( $this->get_msgSuccess() as $value ) {
                                        echo msg_success_default($value);
                                    }
                                }

                                /*
                                |--------------------------------------------------------------------------
                                | Gera um Token de segurança para formulários
                                |--------------------------------------------------------------------------
                                |
                                */
                                WolfFormSecurity::generate_token();
                            ?>

                            <div class="page-block_content">

                                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data" parsley-validate novalidate>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Arqivo CSV*</label>
                                        <div class="col-sm-5">
                                            <input type="file" name="txtCSV" class="form-control" required/>
                                        </div>
                                    </div>

                                    <input type="text" name="sEmail" value="" style="display: none;">
                                    <input type="text" name="sHora" value="<?php echo time(); ?>" style="display: none;">
                                    <input type="hidden" name="sToken" value="<?php echo WolfFormSecurity::get_token(); ?>">

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" name="btnImportarCli" class="btn btn-success">Importar</button>
                                        <button type="reset" class="btn btn-default">Resetar</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <!--End Block-->
                </div>

            </div><!--page-content-->
        </div><!--panel-content-->