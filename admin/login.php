<?php

/**
 * Exibibe a página de login
 *
 * @version 1.0
 */

/*
|--------------------------------------------------------------------------
| Arquivos necessarios para a aplicação funcionar
|  - Arquivo de configuração
|  - Funções principais da aplicação  ( Autoload, etc )
|--------------------------------------------------------------------------
|
*/
if (!file_exists('../config.php'))
	die("Error!! The configuration file can not be found.");

require_once '../config.php'; // Configurações Básicas
require_once ABSPATH.'core/functions/Main.php'; // Funções


/*
|--------------------------------------------------------------------------
| Verifica se o debug para o Admin esta habilitado
|--------------------------------------------------------------------------
|
*/
if ( Config::get('ADMIN_DEBUG') === false ) {

	# Esconde todos os erros
	error_reporting(0);
	ini_set("display_errors", 0);

} else {

	# Mostra todos os erros
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}


/*
|--------------------------------------------------------------------------
| Inicializa uma sessão
|--------------------------------------------------------------------------
|
*/
WolfSession::init();


/*
|--------------------------------------------------------------------------
| Verifica se aplicação encontra-se em manutenção
|--------------------------------------------------------------------------
|
*/
if ( Config::get('ADMIN_MAINTENANCE') === true ) {

	#require_once ABSPATH . '/includes/_templates/maintenance/public.php';
	die("A aplicação ADMIN encontra-se em manutenção!");
	exit();
}


/*
|--------------------------------------------------------------------------
| Executa a aplicação de Login
|--------------------------------------------------------------------------
|
*/

//echo 'OK!!<br>';
/*
echo 'Sessões';
echo '<pre>';
print_r($_SESSION);
echo '</pre><hr>';

echo 'POST';
echo '<pre>';
(isset($_POST)) ? print_r($_POST) : $_POST = array();
echo '</pre>';
*/

require_once Config::get("ABSPATH_CONTROLS_ADMIN") . 'login-control-admin.php'; // Inclui o controle
require_once Config::get("ABSPATH_VIEWS_ADMIN") . 'login/login-view-admin.php'; // Inclui a visão