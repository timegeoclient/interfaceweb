<?php
/**
 * Modelo que manipula os dados do Administrador
 *
 * @version 1.0
 */
final class AdministradorModelAdmin extends MainModelAdmin
{

	/*
    |--------------------------------------------------------------------------
    | Funçoes Primárias
    |--------------------------------------------------------------------------
    |
    */

	/**
	 * Cadastra um Admin
	 */
	public function register_admin()
	{
		# Validação do Token
		if ( !WolfFormSecurity::check_spam_form() ) {

			$this->msgError = array_replace( $this->msgError , array( 'token_form' => 'Erro! Sua solicitação não foi considerada segura. Recarregue a página e tente novamente.' ) );
			unset($_POST);
			return false;
		}

		# Captura dos dados
		$vNOMADM = isset( $_POST['txtNOMADM']) ? $_POST['txtNOMADM'] : NULL; // Nome
		$vEMPADM = isset( $_POST['txtEMPADM']) ? $_POST['txtEMPADM'] : NULL; // Empresa
		$vEMAADM = isset( $_POST['txtEMAADM']) ? $_POST['txtEMAADM'] : NULL; // Email
		$vUSUADM = isset( $_POST['txtUSUADM']) ? $_POST['txtUSUADM'] : NULL; // Usuario
		$vSENADM = isset( $_POST['txtSENADM']) ? $_POST['txtSENADM'] : NULL; // Senha
		$vSENADM2 = isset( $_POST['txtSENADM2']) ? $_POST['txtSENADM2'] : NULL; // Confirmação de Senha

		# Sanatização dos dados
		$vNOMADM = WolfFilter::sanitize_string( mb_strtoupper($vNOMADM, 'UTF-8') );
		$vEMPADM = WolfFilter::sanitize_int( $vEMPADM );
		$vEMAADM = WolfFilter::sanitize_email( $vEMAADM );
		$vUSUADM = WolfFilter::sanitize_string( $vUSUADM );
		$vSENADM = WolfFilter::sanitize_string( $vSENADM );
		$vSENADM2 = WolfFilter::sanitize_string( $vSENADM2 );

		# Validação dos dados
		if ( !WolfFilter::full_required( $vNOMADM ) ) {
			$this->msgError = array_replace( $this->msgError , array( 0 => 'O Nome do admin é obrigatório!' ) );
			return false;

		}elseif ( !WolfFilter::validate_alpha_space( $vNOMADM ) ) {
			$this->msgError = array_replace( $this->msgError , array( 1 => 'O Nome do admin contém caracteres inválidos! Use somente caracteres alfas.' ) );
			return false;

		}elseif ( !WolfFilter::validate_max( $vNOMADM, 200 ) ) {
			$this->msgError = array_replace( $this->msgError , array( 2 => 'O Nome do admin deve ter no máximo 200 caracteres!' ) );
			return false;
		}

		elseif ( !WolfFilter::full_required( $vEMPADM ) ) {
			$this->msgError = array_replace( $this->msgError , array( 3 => 'A Empresa do admin é obrigatório!' ) );
			return false;
		}

		elseif ( !WolfFilter::full_required( $vEMAADM ) ) {
			$this->msgError = array_replace( $this->msgError , array( 4 => 'O Email do admin é obrigatório!' ) );
			return false;

		}elseif ( !WolfFilter::validate_email( $vEMAADM ) ) {
			$this->msgError = array_replace( $this->msgError , array( 5 => 'Informe um email válido!' ) );
			return false;

		}elseif ( !WolfFilter::validate_max( $vEMAADM, 150 ) ) {
			$this->msgError = array_replace( $this->msgError , array( 6 => 'O Email do admin deve ter no máximo 150 caracteres!' ) );
			return false;
		}

		elseif ( !WolfFilter::full_required( $vUSUADM ) ) {
			$this->msgError = array_replace( $this->msgError , array( 7 => 'O Usuário do admin é obrigatório!' ) );
			return false;

		}elseif ( !WolfFilter::validate_alphanumeric( $vUSUADM ) ) {
			$this->msgError = array_replace( $this->msgError , array( 8 => 'O Usuário do admin contém caracteres inválidos! Use somente caracteres alfas e numéricos, sem espaços.' ) );
			return false;

		}

		elseif ( !WolfFilter::full_required( $vSENADM ) ) {
			$this->msgError = array_replace( $this->msgError , array( 9 => 'A Senha do admin é obrigatória!' ) );
			return false;

		}elseif ( $vSENADM == $vUSUADM ) {
			$this->msgError = array_replace( $this->msgError , array( 10 => 'A senha deve ser diferente do nome do usuário!' ) );
			return false;

		}elseif ( !WolfFilter::validate_min( $vSENADM, 8 ) ) {
			$this->msgError = array_replace( $this->msgError , array( 11 => 'A senha deve ter no mímino 8 caracteres!' ) );
			return false;

		}elseif ( !WolfFilter::full_required( $vSENADM2 ) ) {
			$this->msgError = array_replace( $this->msgError , array( 12 => 'Confirme a senha do admin!' ) );
			return false;

		}elseif ( $vSENADM != $vSENADM2 ) {
			$this->msgError = array_replace( $this->msgError , array( 13 => 'Confirmação de senha inválida!' ) );
			return false;

		}

		else{

			WolfConn::db_connect(); // Abre a conexao com o banco de dados && Inicia uma transação
       		$conn = WolfConn::get_db_connection(); // Recupera a conexão ativa com o banco de dados

       		//----------------------------------------------------------------------------
			# Verificando se existe um Admin com o mesmo nome de usuario
			$sql  = 'SELECT id_adm FROM administrador WHERE usuario_adm = :usuario_adm LIMIT 1';

			try {

				$query = $conn->prepare($sql);
				$query->bindParam(':usuario_adm', $vUSUADM, PDO::PARAM_STR);
				$query->execute();

				$result = $query->fetchAll(PDO::FETCH_ASSOC);

			} catch (PDOException $e) {

				# Mostra o erro
	            if ( DEBUG_GLOBAL === true ) { echo $e->getMessage(); }

	            # Salva o log de erro
	            if ( Config::get("LOGGER") === true ) { WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/admin/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' ); }

	            return false;
			}

			// Já existe um admin com esse nome de usuario
			if ( !empty($result) ) {
				$this->msgError = array_replace( $this->msgError , array( 13 => 'Já existe um admin com esse nome de usuario!' ) );
				return false;
			}

			//----------------------------------------------------------------------------
			# Inserindo o admin
			$vSENADM = encrypt_pass( $vSENADM ); // Criptografando a senha

       		$sql  = 'INSERT INTO administrador (usuario_adm, senha_adm, nome_adm, email_adm, id_empresa)
       				VALUES (:usuario_adm, :senha_adm, :nome_adm, :email_adm, :id_empresa)';

			try {

				$query = $conn->prepare($sql);
				$query->bindParam(':usuario_adm', $vUSUADM, PDO::PARAM_STR);
				$query->bindParam(':senha_adm', $vSENADM, PDO::PARAM_STR);
				$query->bindParam(':nome_adm', $vNOMADM, PDO::PARAM_STR);
				$query->bindParam(':email_adm', $vEMAADM, PDO::PARAM_STR);
				$query->bindParam(':id_empresa', $vEMPADM, PDO::PARAM_INT);
				$query->execute();

				# Sucesso :D
				WolfConn::db_commit();

				$this->msgSuccess = array_replace( $this->msgSuccess , array( 1 => 'Admin cadastrado com sucesso!' ) );
				unset($_POST);
				return true;

			} catch (PDOException $e) {

				# Mostra o erro
	            if ( DEBUG_GLOBAL === true ) { echo $e->getMessage(); }

	            # Salva o log de erro
	            if ( Config::get("LOGGER") === true ) { WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/admin/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' ); }

	            # Erro :X
				$this->msgError = array_replace( $this->msgError , array( 14 => 'Aconteceu um erro durante o cadastro do Admin!' ) );
				unset($_POST);
				return false;
			}
		}

	} // register_admin


	/*
    |--------------------------------------------------------------------------
    | Funçoes Secundárias
    |--------------------------------------------------------------------------
    |
    */

	/**
	 * Seleciona todas as empresas cadastradas
	 * @return array
	 */
	public function select_all_empresas()
	{
		WolfConn::db_connect(); // Abre a conexao com o banco de dados && Inicia uma transação
   		$conn = WolfConn::get_db_connection(); // Recupera a conexão ativa com o banco de dados

		$sql  = 'SELECT id_empresa, nome_empresa FROM empresa ORDER BY id_empresa DESC';

		try {

			$query = $conn->prepare($sql);
			$query->execute();

			return ( $result = $query->fetchAll(PDO::FETCH_ASSOC) );

		} catch (PDOException $e) {

			# Mostra o erro
            if ( DEBUG_GLOBAL === true ) { echo $e->getMessage(); }

            # Salva o log de erro
            if ( Config::get("LOGGER") === true ) { WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/admin/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' ); }

            # Erro ao Selecionar :X
            return array();
		}

		WolfConn::db_commit();
	}
}