<?php
/**
 * Modelo que gerencia o acesso do Admin na aplicação
 *
 * @version 1.0
 */
final class LoginModelAdmin extends MainModelAdmin
{

	/*
    |--------------------------------------------------------------------------
    | Funçoes Primárias
    |--------------------------------------------------------------------------
    |
    */


	/**
	 * Método para Logar um usuario
	 */
	public function log_in()
	{
		/*
		* Segurança - AntiSpam
		* Em caso de erro, a aplicação será abortada
		*/

		# Validação do Token
		if ( !WolfFormSecurity::check_spam_form() ) {

			$this->msgError = array_replace( $this->msgError , array( 'token_form' => 'Erro! Sua solicitação não foi considerada segura. Recarregue a página e tente novamente.' ) );
			unset($_POST);
			return false;
		}

		# Captura dos dados
		$vNOMUSU = isset( $_POST['txtNOMUSU']) ? $_POST['txtNOMUSU'] : NULL; // Nome
		$vSENUSU = isset( $_POST['txtSENUSU']) ? $_POST['txtSENUSU'] : NULL; // Senha

		# Sanatização dos dados
		$vNOMUSU = WolfFilter::sanitize_string( $vNOMUSU );
		$vSENUSU = WolfFilter::sanitize_string( $vSENUSU );

		# Validação dos dados
		if ( !WolfFilter::full_required( $vNOMUSU ) ) {
			$this->msgError = array_replace( $this->msgError , array( 0 => 'Nome do usuário é obrigatório!' ) );
			return false;

		}elseif ( !WolfFilter::full_required( $vSENUSU ) ) {
			$this->msgError = array_replace( $this->msgError , array( 1 => 'Senha do usuário é obrigatória!' ) );
			return false;
		}

		// Caso todas as validações sejam um sucesso
		else{

			# Verificando se o Usuário existe
			WolfConn::db_connect(); // Abre a conexao com o banco de dados && Inicia uma transação
       		$conn = WolfConn::get_db_connection(); // Recupera a conexão ativa com o banco de dados

			$sql  = 'SELECT id_adm, usuario_adm, senha_adm FROM administrador WHERE usuario_adm = :usuario_adm LIMIT 1';

			try {

				$query = $conn->prepare($sql);
				$query->bindParam(':usuario_adm', $vNOMUSU, PDO::PARAM_STR);
				$query->execute();

				$result = $query->fetchAll(PDO::FETCH_ASSOC);

			} catch (PDOException $e) {

				# Mostra o erro
	            if ( DEBUG_GLOBAL === true ) {
	                echo $e->getMessage();
	            }

	            # Salva o log de erro
	            if ( Config::get("LOGGER") === true ) {
	                WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/login/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' );
	            }

	            # Erro ao Selecionar :X
	            return false;
			}

			// Se o Usuário não existir
			if ( empty($result) )  {

				WolfConn::db_commit(); // Fecha a conexão e aplica todas as operações da transação

				# Erro :X
				$this->msgError = array_replace( $this->msgError , array( 2 => 'Nome de usuário ou senha estão incorretos!' ) );
				unset($_POST);
				return false;

			}else{

				# Sanatização || Validação dos dados selecionados do banco
				$result[0]['id_adm']  = WolfFilter::sanitize_int( $result[0]['id_adm'] );
				$result[0]['usuario_adm']  = WolfFilter::sanitize_string( $result[0]['usuario_adm'] );
				$result[0]['senha_adm']  = WolfFilter::sanitize_string( $result[0]['senha_adm'] );

				// Criptografando a senha
				$vSENUSU = encrypt_pass( $vSENUSU );

				// Se a senha não for válida
				if ( $result[0]['senha_adm'] != $vSENUSU ) {

					WolfConn::db_commit();

					# Erro :X
					$this->msgError = array_replace( $this->msgError , array( 3 => 'A senha esta incorreta!' ) );
					unset($_POST['txtSENUSU']);
					return false;
				}

				else{

					#-=-=-=- DADOS VÁLIDOS -=-=-=-=- #
					/*
					* Key de segurança que será salvo em uma sessão
					* Hash ( user_agent + senha do bd + SALT ) = 40 caracteres
					*/
					$keyUsu = $_SERVER['HTTP_USER_AGENT'];
					$keyUsu = hash( 'sha1', $keyUsu . $result[0]['senha_adm'] . Config::get("SALT") );

					# Criação das sessões
					WolfSession::set( 'id_adm', $result[0]['id_adm'] );
					WolfSession::set( 'usuario', $result[0]['usuario_adm'] );
					WolfSession::set( 'keyUsu', $keyUsu );
					WolfSession::set( 'tipo', 'adm' );

					WolfConn::db_commit();

		        	# Sucesso :D
					unset($_POST);
					return true;
				}

			}
		}

	} // log_in


	/**
	 * Método para verificar se um Admin esta logado
	 */
	public function logged_on()
	{
		//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		# Se as sessões estiverem setadas
    	if ( $this->sessions_on() ) {

    		# Sanatização dos dados nas sessões
    		$vIDADM = WolfFilter::sanitize_int( $_SESSION['id_adm'] );

    		# Verificando se o Usuário existe
			WolfConn::db_connect(); // Abre a conexao com o banco de dados && Inicia uma transação
       		$conn = WolfConn::get_db_connection(); // Recupera a conexão ativa com o banco de dados

       		$sql  = 'SELECT senha_adm FROM administrador WHERE id_adm = :id_adm LIMIT 1';

			try {

				$query = $conn->prepare($sql);
				$query->bindParam(':id_adm', $vIDADM, PDO::PARAM_INT);
				$query->execute();

				$result = $query->fetchAll(PDO::FETCH_ASSOC);

			} catch (PDOException $e) {
				echo $e->getMessage();
			}

			// Se o Usuário não existir
			if ( empty($result) )  {

				# Erro :X
	            $this->logout();

	            WolfConn::db_commit();
	            return false;
			}

			// Usuário existe
			else{

        		# Sanatização||Validação dos dados selecionados
        		$result[0]['senha_adm']  = WolfFilter::sanitize_string( $result[0]['senha_adm'] );

        		$keyUsu = $_SERVER['HTTP_USER_AGENT'];
				$keyUsu = hash( 'sha1', $keyUsu . $result[0]['senha_adm'] . Config::get("SALT") );

				// Se a Key de Usuário estiver inválido
        		if ( $_SESSION['keyUsu'] != $keyUsu ) {

        			# Erro :X
		            $this->logout();

		            WolfConn::db_commit();
		            return false;
        		}

        		else{

                    WolfConn::db_commit();

                    # Sucesso :D
                    return true;
        		}
        	}
    	}

    	// Sessões não setadas
    	else{
    		# Erro :X
			return false;
    	}
	}


	/**
	 * Desloga o Usuário
	 */
	public function logout()
	{
		if ( !isset($_SESSION) ) {
	        WolfSession::init();
	    }

	    # Destrói as sessões
	    WolfSession::destroy_all();
	}



	/*
    |--------------------------------------------------------------------------
    | Funçoes Secundárias
    |--------------------------------------------------------------------------
    |
    */

	/**
	 * Checa se as sessões do login do Usuário estão setadas
	 * @return bool
	 */
	private function sessions_on()
	{
	    if ( isset($_SESSION['id_adm']) && isset($_SESSION['usuario']) && isset($_SESSION['keyUsu']) && isset($_SESSION['tipo']) )
	        return true;
	    else
	        return false;
	}
}