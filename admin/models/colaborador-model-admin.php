<?php
/**
 * Modelo que manipula os dados do Colaborador
 *
 * @version 1.0
 */
final class ColaboradorModelAdmin extends MainModelAdmin
{
	/*
    |--------------------------------------------------------------------------
    | Funçoes Primárias
    |--------------------------------------------------------------------------
    |
    */

	/**
	 * Cadastra um Admin
	 */
	public function register_collaborator()
	{
		# Validação do Token
		if ( !WolfFormSecurity::check_spam_form() ) {

			$this->msgError = array_replace( $this->msgError , array( 'token_form' => 'Erro! Sua solicitação não foi considerada segura. Recarregue a página e tente novamente.' ) );
			unset($_POST);
			return false;
		}

		# Captura dos dados
		$vNOMCOL = isset( $_POST['txtNOMCOL']) ? $_POST['txtNOMCOL'] : NULL; // Nome
		$vEMPCOL = isset( $_POST['txtEMPCOL']) ? $_POST['txtEMPCOL'] : NULL; // Empresa
		$vEMACOL = isset( $_POST['txtEMACOL']) ? $_POST['txtEMACOL'] : NULL; // Email
		$vUSUCOL = isset( $_POST['txtUSUCOL']) ? $_POST['txtUSUCOL'] : NULL; // Usuario
		$vSENCOL = isset( $_POST['txtSENCOL']) ? $_POST['txtSENCOL'] : NULL; // Senha
		$vSENCOL2 = isset( $_POST['txtSENCOL2']) ? $_POST['txtSENCOL2'] : NULL; // Confirmação de Senha

		$vMACCOL = isset( $_POST['txtMACCOL']) ? $_POST['txtMACCOL'] : NULL; // MAC
		$vSOCOL  = isset( $_POST['txtSOCOL']) ? $_POST['txtSOCOL'] : NULL; // Sistema Operacional
		$vVERCOL = isset( $_POST['txtVERCOL']) ? $_POST['txtVERCOL'] : NULL; // Versão do SO
		$vNUMCOL = isset( $_POST['txtNUMCOL']) ? $_POST['txtNUMCOL'] : NULL; // Número

		# Sanatização dos dados
		$vNOMCOL = WolfFilter::sanitize_string( mb_strtoupper($vNOMCOL, 'UTF-8') );
		$vEMPCOL = WolfFilter::sanitize_int( $vEMPCOL );
		$vEMACOL = WolfFilter::sanitize_email( $vEMACOL );
		$vUSUCOL = WolfFilter::sanitize_string( $vUSUCOL );
		$vSENCOL = WolfFilter::sanitize_string( $vSENCOL );
		$vSENCOL2 = WolfFilter::sanitize_string( $vSENCOL2 );

		$vMACCOL = WolfFilter::sanitize_string( $vMACCOL );
		$vSOCOL = WolfFilter::sanitize_string( $vSOCOL );
		$vVERCOL = WolfFilter::sanitize_string( $vVERCOL );
		$vNUMCOL = WolfFilter::sanitize_int( $vNUMCOL ); // Somente numeros

		# Validação dos dados
		if ( !WolfFilter::full_required( $vNOMCOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 0 => 'O Nome do colaborador é obrigatório!' ) );
			return false;

		}elseif ( !WolfFilter::validate_alpha_space( $vNOMCOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 1 => 'O Nome do colaborador contém caracteres inválidos! Use somente caracteres alfas.' ) );
			return false;

		}elseif ( !WolfFilter::validate_max( $vNOMCOL, 200 ) ) {
			$this->msgError = array_replace( $this->msgError , array( 2 => 'O Nome do colaborador deve ter no máximo 200 caracteres!' ) );
			return false;
		}

		elseif ( !WolfFilter::full_required( $vEMPCOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 3 => 'A Empresa do colaborador é obrigatório!' ) );
			return false;
		}

		elseif ( !WolfFilter::full_required( $vEMACOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 4 => 'O Email do colaborador é obrigatório!' ) );
			return false;

		}elseif ( !WolfFilter::validate_email( $vEMACOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 5 => 'Informe um email válido!' ) );
			return false;

		}elseif ( !WolfFilter::validate_max( $vEMACOL, 150 ) ) {
			$this->msgError = array_replace( $this->msgError , array( 6 => 'O Email do colaborador deve ter no máximo 150 caracteres!' ) );
			return false;
		}

		elseif ( !WolfFilter::full_required( $vUSUCOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 7 => 'O Usuário do colaborador é obrigatório!' ) );
			return false;

		}elseif ( !WolfFilter::validate_alphanumeric( $vUSUCOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 8 => 'O Usuário do colaborador contém caracteres inválidos! Use somente caracteres alfas e numéricos, sem espaços.' ) );
			return false;

		}

		elseif ( !WolfFilter::full_required( $vSENCOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 9 => 'A Senha do colaborador é obrigatória!' ) );
			return false;

		}elseif ( $vSENCOL == $vUSUCOL ) {
			$this->msgError = array_replace( $this->msgError , array( 10 => 'A senha deve ser diferente do nome do usuário!' ) );
			return false;

		}elseif ( !WolfFilter::validate_min( $vSENCOL, 8 ) ) {
			$this->msgError = array_replace( $this->msgError , array( 11 => 'A senha deve ter no mímino 8 caracteres!' ) );
			return false;

		}elseif ( !WolfFilter::full_required( $vSENCOL2 ) ) {
			$this->msgError = array_replace( $this->msgError , array( 12 => 'Confirme a senha do colaborador!' ) );
			return false;

		}elseif ( $vSENCOL != $vSENCOL2 ) {
			$this->msgError = array_replace( $this->msgError , array( 13 => 'Confirmação de senha inválida!' ) );
			return false;

		}

		elseif ( !WolfFilter::full_required( $vMACCOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 14 => 'O endereço MAC é obrigatório!' ) );
			return false;

		}

		elseif ( !WolfFilter::full_required( $vSOCOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 15 => 'O Sistema Operacional é obrigatório!' ) );
			return false;

		}elseif ( !WolfFilter::validate_max( $vSOCOL, 30 ) ) {
			$this->msgError = array_replace( $this->msgError , array( 16 => 'O Sistema Operacional deve ter no máximo 30 caracteres!' ) );
			return false;
		}

		elseif ( !WolfFilter::full_required( $vVERCOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 17 => 'A versão do SO é obrigatório!' ) );
			return false;

		}elseif ( !WolfFilter::validate_max( $vVERCOL, 10 ) ) {
			$this->msgError = array_replace( $this->msgError , array( 18 => 'A versão do SO deve ter no máximo 10 caracteres!' ) );
			return false;

		}elseif ( !WolfFilter::full_required( $vNUMCOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 19 => 'O número do  colaborador é obrigatório!' ) );
			return false;

		}elseif ( !WolfFilter::validate_max( $vNUMCOL, 20 ) ) {
			$this->msgError = array_replace( $this->msgError , array( 20 => 'O número deve ter no máximo 20 caracteres!' ) );
			return false;

		}elseif ( !WolfFilter::validate_numeric( $vNUMCOL ) ) {
			$this->msgError = array_replace( $this->msgError , array( 21 => 'O número deve conter somete caracteres númericos!' ) );
			return false;
		}

		else{

			WolfConn::db_connect(); // Abre a conexao com o banco de dados && Inicia uma transação
       		$conn = WolfConn::get_db_connection(); // Recupera a conexão ativa com o banco de dados

       		//----------------------------------------------------------------------------
			# Verificando se existe um Colaborador com o mesmo nome de usuario
			$sql  = 'SELECT id_colab FROM colaborador WHERE usuario_colab = :usuario_colab LIMIT 1';

			try {

				$query = $conn->prepare($sql);
				$query->bindParam(':usuario_colab', $vUSUCOL, PDO::PARAM_STR);
				$query->execute();

				$result = $query->fetchAll(PDO::FETCH_ASSOC);

			} catch (PDOException $e) {

				# Mostra o erro
	            if ( DEBUG_GLOBAL === true ) { echo $e->getMessage(); }

	            # Salva o log de erro
	            if ( Config::get("LOGGER") === true ) { WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/admin/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' ); }

	            return false;
			}


			// Já existe um colaborador com esse nome de usuario
			if ( !empty($result) ) {
				$this->msgError = array_replace( $this->msgError , array( 13 => 'Já existe um colaborador com esse nome de usuario!' ) );
				return false;
			}


       		//----------------------------------------------------------------------------
			# Inserindo o colaborador
			$vSENCOL = encrypt_pass( $vSENCOL ); // Criptografando a senha
			$vHASVER = hash('sha1', mt_rand()); // Chave de verificação : 40 caracteres

       		$sql  = 'INSERT INTO colaborador (usuario_colab, senha_colab, nome_colab, email_colab, id_empresa, mac_colab, so_colab, versao_colab, numero_colab, validacao_colab, chave_verif)
       				VALUES (:usuario_colab, :senha_colab, :nome_colab, :email_colab, :id_empresa, :mac_colab, :so_colab, :versao_colab, :numero_colab, :validacao_colab, :chave_verif)';

			try {

				$query = $conn->prepare($sql);
				$query->bindParam(':usuario_colab', $vUSUCOL, PDO::PARAM_STR);
				$query->bindParam(':senha_colab', $vSENCOL, PDO::PARAM_STR);
				$query->bindParam(':nome_colab', $vNOMCOL, PDO::PARAM_STR);
				$query->bindParam(':email_colab', $vEMACOL, PDO::PARAM_STR);
				$query->bindParam(':id_empresa', $vEMPCOL, PDO::PARAM_INT);
				$query->bindParam(':mac_colab', $vMACCOL, PDO::PARAM_STR);
				$query->bindParam(':so_colab', $vSOCOL, PDO::PARAM_STR);
				$query->bindParam(':versao_colab', $vVERCOL, PDO::PARAM_STR);
				$query->bindParam(':numero_colab', $vNUMCOL, PDO::PARAM_INT);
				$query->bindValue(':validacao_colab', false, PDO::PARAM_BOOL);
				$query->bindParam(':chave_verif', $vHASVER, PDO::PARAM_STR);
				$query->execute();

			} catch (PDOException $e) {

				# Mostra o erro
	            if ( DEBUG_GLOBAL === true ) { echo $e->getMessage(); }

	            # Salva o log de erro
	            if ( Config::get("LOGGER") === true ) { WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/colaborador/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' ); }

	            # Erro :X
				$this->msgError = array_replace( $this->msgError , array( 22 => 'Aconteceu um erro durante o cadastro do Colaborador!' ) );
				unset($_POST);
				return false;
			}

			//-------------------------------------------
			# Enviando email para verificação de conta
			$link = Config::get("COL_CONFIRM_URL").'?key='.urlencode($vHASVER); // Link com a chave de verificação

			// Define a mensagem
		    $message = file_get_contents( Config::get("ABSPATH_CORE_TEMPLATE") . 'email/col-confirm.html' ); // Template personalizado
		    $message = str_replace('#NOME_DESTINATARIO#', $vNOMCOL, $message);
		    $message = str_replace('#LINK_VERIFICACAO#', $link, $message);

			$data_email = array(
			    'EMAIL_USE_SMTP' => true,
			    'FromEmail'      => 'appfran10@gamil.com',
			    'FromName'       => 'GeoClient',
			    'AddressEmail'   => $vEMACOL,
			    'AddressName'    => $vNOMCOL,
			    'subject'        => 'Verificação de Conta - GeoClient',
			    'messageBody'    => $message,
			    'messageAltBody' => "Sua conta como Colaborador foi cadastrada com sucesso no site GeoClient. Mas para que você possa esta utilzando a sua conta, você deve verificá-la clicando no link a seguir: $link"
		    );

			// Email enviado com sucesso
			if ( WolfEmail::send_email( $data_email ) ) {

				# Fecha a conexão e aplica todas as operações da transação
				WolfConn::db_commit();

				# Sucesso :D
				//$this->msgSuccess = array_replace( $this->msgSuccess , array( 1 => $link ) );
				$this->msgSuccess = array_replace( $this->msgSuccess , array( 2 => 'Colaborador cadastrado com sucesso!' ) );
				unset($_POST);
				return true;
			}

			// Erro no envio de email de verificação
			else{

				# Fecha a conexão e desfaz as operações da transação
				WolfConn::db_rollback();

				# Erro :X
				$this->msgError = array_replace( $this->msgError , array( 23 => 'O Colaborador não pode ser cadastrado, devido a falha do envio de email de verificação de conta!' ) );
				unset($_POST);
				return false;
			}
		}

	} // register_collaborator


	/*
    |--------------------------------------------------------------------------
    | Funçoes Secundárias
    |--------------------------------------------------------------------------
    |
    */

	/**
	 * Seleciona todas as empresas cadastradas
	 * @return array
	 */
	public function select_all_empresas()
	{
		WolfConn::db_connect(); // Abre a conexao com o banco de dados && Inicia uma transação
   		$conn = WolfConn::get_db_connection(); // Recupera a conexão ativa com o banco de dados

		$sql  = 'SELECT id_empresa, nome_empresa FROM empresa ORDER BY id_empresa DESC';

		try {

			$query = $conn->prepare($sql);
			$query->execute();

			return ( $result = $query->fetchAll(PDO::FETCH_ASSOC) );

		} catch (PDOException $e) {

			# Mostra o erro
            if ( DEBUG_GLOBAL === true ) { echo $e->getMessage(); }

            # Salva o log de erro
            if ( Config::get("LOGGER") === true ) { WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/login/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' ); }

            # Erro ao Selecionar :X
            return array();
		}

		WolfConn::db_commit();
	}


	/**
	 * Faz a verificação de conta do colaborador
	 */
	public function verification_colab()
	{
		# Captura dos dados
	    $vHASVER = isset( $_GET['key']) ? $_GET['key'] : NULL;

	    # Sanatização
	    $vHASVER = WolfFilter::sanitize_string( $vHASVER );

	    # Validação dos dados
	    if ( !WolfFilter::full_required( $vHASVER ) ) {
	        $this->msgError = array_replace( $this->msgError , array( 1 => 'Chave de verificação não informado!' ) );
	    	return false;

	    }else{

	    	# Verificando a conta associada a chave
			WolfConn::db_connect(); // Abre a conexao com o banco de dados && Inicia uma transação
       		$conn = WolfConn::get_db_connection(); // Recupera a conexão ativa com o banco de dados

			$sql  = 'SELECT id_colab FROM colaborador WHERE chave_verif = :chave_verif LIMIT 1';

			try {

				$query = $conn->prepare($sql);
				$query->bindParam(':chave_verif', $vHASVER, PDO::PARAM_STR);
				$query->execute();

				$result = $query->fetchAll(PDO::FETCH_ASSOC);

			} catch (PDOException $e) {

				# Mostra o erro
	            if ( DEBUG_GLOBAL === true ) { echo $e->getMessage(); }

	            # Salva o log de erro
	            if ( Config::get("LOGGER") === true ) { WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/colaborador/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' ); }

	            # Erro ao Selecionar :X
	            return false;
			}

			// Se o colaborador não existir
			if ( empty($result) )  {

				WolfConn::db_commit(); // Fecha a conexão e aplica todas as operações da transação

				# Erro :X
				$this->msgError = array_replace( $this->msgError , array( 2 => 'Código de verificação inválido ou a conta em questão já foi confirmada!' ) );
				unset($_POST);
				return false;
			}

			else{

				// Atualizando o campo 'validacao_colab' = TRUE && 'chave_verif' = null
       			$sql  = 'UPDATE colaborador
       					SET validacao_colab = :validacao_colab , chave_verif = :chave_verif
       					WHERE id_colab = :id_colab';

				try {

					$query = $conn->prepare($sql);
					$query->bindValue(':validacao_colab', TRUE, PDO::PARAM_BOOL);
					$query->bindValue(':chave_verif', NULL, PDO::PARAM_NULL);
					$query->bindParam(':id_colab', $result[0]['id_colab'], PDO::PARAM_INT);
					$query->execute();

					# Sucesso :D
					WolfConn::db_commit();

					$this->msgSuccess = array_replace( $this->msgSuccess , array( 1 => 'Colaborador verificado com sucesso!' ) );
					unset($_POST);
					return true;

				} catch (PDOException $e) {

					# Mostra o erro
		            if ( DEBUG_GLOBAL === true ) { echo $e->getMessage(); }

		            # Salva o log de erro
		            if ( Config::get("LOGGER") === true ) { WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/admin/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' ); }

		            # Fecha a conexão e desfaz as operações da transação
					WolfConn::db_rollback();

		            # Erro :X
					$this->msgError = array_replace( $this->msgError , array( 3 => 'Aconteceu um erro durante a verificação de conta!' ) );
					unset($_POST);
					return false;
				}
			}
	    }
	}
}