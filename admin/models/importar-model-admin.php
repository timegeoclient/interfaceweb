<?php
/**
 * Modelo que manipula os dados de importação
 *
 * @version 1.0
 */
final class ImportarModelAdmin extends MainModelAdmin
{
	/*
    |--------------------------------------------------------------------------
    | Funçoes Primárias
    |--------------------------------------------------------------------------
    |
    */

    /**
	 * Importa uma lista de clientes de um arquivo CSV para o banco
	 */
	public function import_clients()
	{
		# Validação do Token
		if ( !WolfFormSecurity::check_spam_form() ) {

			$this->msgError = array_replace( $this->msgError , array( 'token_form' => 'Erro! Sua solicitação não foi considerada segura. Recarregue a página e tente novamente.' ) );
			unset($_POST);
			return false;
		}

		# Captura dos dados
		$vCSV = isset( $_FILES['txtCSV']) ? $_FILES['txtCSV'] : NULL;

		# Validação dos dados
		$extensoes = array('csv'); // Extensões suportadas

		// pegando a extensão do arquivo
		$value = explode(".", $vCSV['name']);
		$ext = strtolower(array_pop($value));

		if ( !WolfFilter::full_required( $vCSV['name'] ) || $vCSV['error'] <> '0' ){
			$this->msgError = array_replace( $this->msgError , array( 1 => 'Um arquivo .CSV deve ser escolhido!' ) );
			return false;

		}elseif ( array_search($ext, $extensoes) === false ) {
			$this->msgError = array_replace( $this->msgError , array( 2 => 'Formato de arquivo não suportado!' ) );
			return false;
		}

		else{

			if( ($data = read_CSV( $vCSV['tmp_name'] )) == false ){
				$this->msgError = array_replace( $this->msgError , array( 3 => 'O arquivo csv esta mal formatado!' ) );
				return false;
			}

			/*echo '<pre>';
			print_r($data);
			echo '</pre><hr>';*/

			WolfConn::db_connect(); // Abre a conexao com o banco de dados && Inicia uma transação
       		$conn = WolfConn::get_db_connection(); // Recupera a conexão ativa com o banco de dados

			/*
			* Comando SQL
			* 1º GRAVANDO OS DADOS
			*/
			/*$sql = "COPY localizacao
			(
			    nome_cliente,
			    logradoro_cliente,
			    numero_cliente,
			    complemento_cliente,
			    bairro_cliente,
			    cep_cliente,
			    cidade_cliente,
			    estado_cliente,
			    rua_cliente,
			    longitude_cliente,
			    latitude_cliente,
			    satelites_ativos,
			    precisao_loca,
			    id_empresa,
			    id_colab
			)
			FROM '".$vCSV['tmp_name']."'
			DELIMITER ','
			CSV HEADER";
			*/


			$sql = 'INSERT INTO localizacao 
	        (nome_cliente,
	        logradoro_cliente,
	        numero_cliente,
	        complemento_cliente,
	        bairro_cliente,
	        cep_cliente,
	        cidade_cliente,
	        estado_cliente,
	        rua_cliente,
	        longitude_cliente,
	        latitude_cliente,
	        satelites_ativos,
	        precisao_loca,
	        id_empresa,
	        id_colab) 
	        VALUES ';

	        // API para buscar LAT e LON
	        $geo = new Geocoding();

			foreach ($data as $key => $row) {

				// Endereço do cliente
				$endereco = $data[$key]['cidade_cliente'] . ', '. $data[$key]['estado_cliente'] . ', ' . $data[$key]['rua_cliente'];
			    //echo $endereco . '<br>';

				// Realiza a consulta da API
			    $result = $geo->get_lat_lng( $endereco );

			    // Salvando os dados obtidos
			    $data[$key]['longitude_cliente'] = $result['lng'];
			    $data[$key]['latitude_cliente'] = $result['lat'];
			    $data[$key]['precisao_loca'] = ( !empty($data[$key]['longitude_cliente']) && !empty($data[$key]['longitude_cliente']) ) ? 'Baixa' : 'Nenhuma';

			    // Verificando os tipos de dados
			    $data[$key]['nome_cliente'] = WolfFilter::sanitize_string(mb_strtoupper( $data[$key]['nome_cliente'], 'UTF-8' ) );
			    $data[$key]['logradoro_cliente'] = WolfFilter::sanitize_string(mb_strtoupper( $data[$key]['logradoro_cliente'], 'UTF-8' ) );
			    $data[$key]['numero_cliente'] = WolfFilter::sanitize_string(mb_strtoupper( $data[$key]['numero_cliente'], 'UTF-8' ) );
			    $data[$key]['complemento_cliente'] = WolfFilter::sanitize_string(mb_strtoupper( $data[$key]['complemento_cliente'], 'UTF-8' ) );
			    $data[$key]['bairro_cliente'] = WolfFilter::sanitize_string(mb_strtoupper( $data[$key]['bairro_cliente'], 'UTF-8' ) );
			    $data[$key]['cep_cliente'] = WolfFilter::sanitize_string(mb_strtoupper( $data[$key]['cep_cliente'], 'UTF-8' ) );
			    $data[$key]['cidade_cliente'] = WolfFilter::sanitize_string(mb_strtoupper( $data[$key]['cidade_cliente'], 'UTF-8' ) );
			    $data[$key]['estado_cliente'] = WolfFilter::sanitize_string(mb_strtoupper( $data[$key]['estado_cliente'], 'UTF-8' ) );
			    $data[$key]['rua_cliente'] = WolfFilter::sanitize_string(mb_strtoupper( $data[$key]['rua_cliente'], 'UTF-8' ) );
			    $data[$key]['longitude_cliente'] = ( !empty( $data[$key]['longitude_cliente'] ) ) ? WolfFilter::sanitize_string( $data[$key]['longitude_cliente'], 'UTF-8' ) : 'NULL';
			    $data[$key]['latitude_cliente'] = ( !empty( $data[$key]['latitude_cliente'] ) ) ? WolfFilter::sanitize_string( $data[$key]['latitude_cliente'], 'UTF-8' ) : 'NULL';
			    $data[$key]['satelites_ativos'] = ( !empty( $data[$key]['satelites_ativos'] ) ) ? WolfFilter::sanitize_string( $data[$key]['satelites_ativos'], 'UTF-8' ) : '0';
			    $data[$key]['precisao_loca'] = ( !empty($data[$key]['precisao_loca']) ) ? WolfFilter::sanitize_string(mb_strtoupper( $data[$key]['precisao_loca'], 'UTF-8' ) ) : 'NULL';
			    $data[$key]['id_empresa'] = WolfFilter::sanitize_int($data[$key]['id_empresa']);
			    $data[$key]['id_colab'] = WolfFilter::sanitize_int($data[$key]['id_colab']);

			    $sql .= "('".$data[$key]['nome_cliente']."', '".$data[$key]['logradoro_cliente']."', '".$data[$key]['numero_cliente']."', '".$data[$key]['complemento_cliente']."', '".$data[$key]['bairro_cliente']."', '".$data[$key]['cep_cliente']."', '".$data[$key]['cidade_cliente']."', '".$data[$key]['estado_cliente']."', '".$data[$key]['rua_cliente']."', '".$data[$key]['longitude_cliente']."', '".$data[$key]['latitude_cliente']."', '".$data[$key]['satelites_ativos']."', '".$data[$key]['precisao_loca']."', ".$data[$key]['id_empresa'].", ".$data[$key]['id_colab']."), ";
			}

			// Tira o último caractere (vírgula extra)
			$sql = substr(trim($sql), 0, -1);

			/*echo '<pre>';
			print_r($sql);
			echo '</pre>';*/

			try {

			    $query = $conn->prepare($sql);
			    $query->execute();

			    /*
				* Comando SQL
				* 2º Retira os dados duplicados (nome_cliente)
				*/
				$sql= 'DELETE FROM localizacao
						WHERE nome_cliente IN
						  (SELECT nome_cliente FROM localizacao
						   GROUP BY nome_cliente
						   HAVING Count(nome_cliente)>1)
						AND NOT id_loca IN
						  (SELECT Min(id_loca) FROM localizacao
						   GROUP BY nome_cliente
						   HAVING Count(nome_cliente)>1)';

				try {

				    $query = $conn->prepare($sql);
				    $query->execute();

				    # Sucesso :D
					WolfConn::db_commit();

					$this->msgSuccess = array_replace( $this->msgSuccess , array( 1 => 'Clientes importados com sucesso!' ) );
					unset($_POST);
					return true;

				}

				// Erro - 2º SQL
				catch (PDOException $e) {

				    # Mostra o erro
		            if ( DEBUG_GLOBAL === true ) { echo $e->getMessage(); }

		            # Salva o log de erro
		            if ( Config::get("LOGGER") === true ) { WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/importar/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' ); }

		            # Erro :X
					$this->msgError = array_replace( $this->msgError , array( 11 => 'Aconteceu um erro durante a importação! A Comunicação com o banco de dados falhou.' ) );
					unset($_POST);
					return false;
				}

			} 

			// Erro - 1º SQL
			catch (PDOException $e) {

			    # Mostra o erro
	            if ( DEBUG_GLOBAL === true ) { echo $e->getMessage(); }

	            # Salva o log de erro
	            if ( Config::get("LOGGER") === true ) { WolfLogger::write( Config::get("ABSPATH_LOGS") . 'erros/importar/', 'Classe ' . __CLASS__  . ': ' . $e->getMessage(), 'html' ); }

	            # Erro :X
				$this->msgError = array_replace( $this->msgError , array( 10 => 'Aconteceu um erro durante a importação! A Comunicação com o banco de dados falhou ou existem campos importantes no csv não preenchidos.' ) );
				unset($_POST);
				return false;
			}
		}
	}
}